<%@page import="model.bean.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Checkout</title>
</head>
<body>
	<%
		String user = (String)session.getAttribute("user");
		if(user==null){
			response.sendRedirect(request.getContextPath()+"/UserControllerServlet?command=Login");
		}
	%>
	
	<jsp:include page="/templates/public/inc/header.jsp"></jsp:include>
	<div class="container">
		<div class="account">
			<h2 class="account-in">CheckOut</h2>
			<form action="CheckOutServlet" method="post">
				<%
					if(request.getAttribute("error")!=null){
				%>	
				<div>
					<p style= "color:red"><%=request.getAttribute("error")%></p>
				</div>
				<% 
					}
				%>
				<div>
					<span class="name-in">Address*</span> 
					<input type="text" name="address" required>
					<span id="user-result"></span>
				</div>
				<div>
					<span class="name-in">Payment*</span> 
					<select name="payment">
						<option value="BankTranfer">Bank Tranfer</option>
						<option value="Live">Live</option>
						<option value="CardMobile">Card Mobile</option>
					</select>
				</div>
				<input type="hidden" value="CheckOut" name="command"> 
				<input type="submit" value="CheckOut">
			</form>
		</div>
	</div>
	<jsp:include page="/templates/public/inc/footer.jsp"></jsp:include>
</body>
</html>