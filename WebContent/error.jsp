<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Error</title>
</head>
<body>
	<jsp:include page="/templates/public/inc/header.jsp"></jsp:include>
	<div class="container">
		<div class="account">
			<h2 class="account-in">Message</h2>
			<%
				if (request.getAttribute("error") != null) {
			%>
			<div>
				<p style="color: red"><%=request.getAttribute("error")%></p>
			</div>
			<%
				}
			%>
		</div>
	</div>
	<jsp:include page="/templates/public/inc/footer.jsp"></jsp:include>
</body>
</html>