<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/templates/admin/css/mos-style.css">
</head>
<body>
	<div id="header">
		<div class="inHeaderLogin"></div>
	</div>

	<div id="loginForm">
		<div class="headLoginForm">Login Administrator</div>
		<div class="fieldLogin">
			<form method="POST" action="<%=request.getContextPath() %>/AdminLoginServlet">
				<label>Username</label><br> <input type="text" class="login" name="username" required><br> 
				<label>Password</label><br> <input type="password" class="login" name="password" required><br>
				<input type="hidden" value="Login" name="command">
				<input type="submit" class="button" value="Login">
			</form>
		</div>
	</div>
</body>
</html>