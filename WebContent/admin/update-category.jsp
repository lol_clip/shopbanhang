<%@page import="model.bean.Category"%>
<%@page import="model.bean.ListCategory"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update category</title>
</head>
<body>
	<%
		ArrayList<ListCategory> alListCategory = (ArrayList<ListCategory>) request.getAttribute("alListCategory");
		Category category = (Category)request.getAttribute("category");
	%>
	<jsp:include page="/templates/admin/inc/header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="/templates/admin/inc/menuleft.jsp"></jsp:include>
		<div id="rightContent">
			<h3>Cập nhật danh mục</h3>
			<form action="AdminManagerCategoryServlet" method="post">
				<table width="95%">
					<tr>
						<td><input type="hidden"  name="maDanhMuc" value="<%=category.getMaDanhMuc()%>"></td>
					</tr>
					<tr>
						<td><b>Tên danh mục</b></td>
						<td><input type="text" class="sedang" required name="tenDanhMuc" value="<%=category.getTenDanhMuc()%>"></td>
					</tr>

					<tr>
						<td><b>Thể Loại</b></td>
						<td><select name="theLoai">
								<%
									if (alListCategory != null) {
										for (ListCategory listCategory : alListCategory) {
											if(listCategory.getMaDSDanhMuc()==category.getMaDSDanhMuc()){
								%>
								<option selected="selected" value="<%=listCategory.getMaDSDanhMuc()%>"><%=listCategory.getTenDSDanhMuc()%></option>
								<%
										}else{
								%>
								<option value="<%=listCategory.getMaDSDanhMuc()%>"><%=listCategory.getTenDSDanhMuc()%></option>
								<%
									}
									}
									}
								%>
						</select></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" class="button" value="Update"
							name="sua"> <input type="reset" class="button"
							value="Reset"> <input type="button" class="button"
							value="Back" onclick="window.location.href = 'http://localhost:8080/WebBanHang/AdminManagerCategoryServlet?command=DanhMuc'"><input type="hidden"
							value="updateCategory" name="command"></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="clear"></div>
		<jsp:include page="/templates/admin/inc/footer.jsp"></jsp:include>
</body>
</html>