<%@page import="model.bean.Category"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert product</title>
</head>
<body>
	<%
		ArrayList<Category> alCategory = (ArrayList<Category>) request.getAttribute("alCategory");
	%>
	<jsp:include page="/templates/admin/inc/header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="/templates/admin/inc/menuleft.jsp"></jsp:include>
		<div id="rightContent">
			<h3>Thêm sản phẩm mới</h3>
			<form method="post"
				action="<%=request.getContextPath()%>/AdminManagerProductServlet">
				<table width="95%">
					<tr>
						<td><b>Tên sản phẩm(*)</b></td>
						<td><input type="text" class="sedang" name="tenSanPham"
							required></td>
					</tr>
					<tr>
						<td><b>Hình ảnh(*)</b></td>
						<td><input type="text" class="sedang" name="hinhAnh" required>
							<input type="button" name="chonAnh" value="Chọn ảnh"
							onclick="window.location.href = 'http://localhost:8080/WebBanHang/UploadFileServlet'">
						</td>
					</tr>
					<tr>
						<td><b>Giá sản phẩm(*)</b></td>
						<td><input type="text" class="sedang" name="giaSanPham"></td>
					</tr>
					<tr>
						<td><b>Mô tả(*)</b></td>
						<td><textarea name="moTa"></textarea></td>
					</tr>
					<tr>
						<td><b>Thể loại</b></td>
						<td><select name="theLoai">
								<%
									if (alCategory != null) {
										for (Category category : alCategory) {
								%>
								<option value="<%=category.getMaDanhMuc()%>"><%=category.getTenDanhMuc()%></option>
								<%
									}
									}
								%>
						</select></td>
					</tr>

					<tr>
						<td></td>
						<td><input type="submit" class="button" value="Add"
							name="them"> <input type="reset" class="button"
							value="Reset"> <input type="button" class="button"
							value="Back"
							onclick="window.location.href = 'http://localhost:8080/WebBanHang/AdminManagerProductServlet?command=SanPham'"><input
							type="hidden" value="insertProduct" name="command"></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="clear"></div>
		<jsp:include page="/templates/admin/inc/footer.jsp"></jsp:include>
</body>
</html>