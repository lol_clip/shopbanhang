<%@page import="model.bean.Category"%>
<%@page import="model.bean.ListCategory"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update category</title>
</head>
<body>
	<%
		ListCategory menubar = (ListCategory) request.getAttribute("menuBar");
	%>
	<jsp:include page="/templates/admin/inc/header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="/templates/admin/inc/menuleft.jsp"></jsp:include>
		<div id="rightContent">
			<h3>Cập nhật MenuBar</h3>
			<form action="AdminManagerCategoryListServlet" method="post">
				<table width="95%">
					<tr>
						<td><input type="hidden"  name="maMenuBar" value="<%=menubar.getMaDSDanhMuc()%>"></td>
					</tr>
					<tr>
						<td><b>Tên MenuBar</b></td>
						<td><input type="text" class="sedang" required name="tenMenuBar" value="<%=menubar.getTenDSDanhMuc()%>"></td>
					</tr>

					<tr>
						<td></td>
						<td><input type="submit" class="button" value="Update"
							name="sua"> <input type="reset" class="button"
							value="Reset"> <input type="button" class="button"
							value="Back" onclick="window.location.href = 'http://localhost:8080/WebBanHang/AdminManagerCategoryListServlet?command=Menubar'"><input type="hidden"
							value="updateCategoryList" name="command"></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="clear"></div>
		<jsp:include page="/templates/admin/inc/footer.jsp"></jsp:include>
</body>
</html>