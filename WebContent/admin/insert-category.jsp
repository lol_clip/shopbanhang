<%@page import="model.bean.ListCategory"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert category</title>
</head>
<body>
	<%
		ArrayList<ListCategory> alListCategory = (ArrayList<ListCategory>) request.getAttribute("alListCategory");
	%>
	<jsp:include page="/templates/admin/inc/header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="/templates/admin/inc/menuleft.jsp"></jsp:include>
		<div id="rightContent">
			<h3>Thêm danh mục mới</h3>
			<form action="AdminManagerCategoryServlet" method="post">
				<table width="95%">
					<tr>
						<td><b>Tên danh mục</b></td>
						<td><input type="text" class="sedang" name="tenDanhMuc" required></td>
					</tr>

					<tr>
						<td><b>Thể Loại</b></td>
						<td><select name="theLoai">
								<%
									if (alListCategory != null) {
										for (ListCategory listCategory : alListCategory) {
								%>
								<option value="<%=listCategory.getMaDSDanhMuc()%>"><%=listCategory.getTenDSDanhMuc()%></option>
								<%
									}
									}
								%>
						</select></td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" class="button" value="Add"
							name="them"> <input type="reset" class="button"
							value="Reset"> <input type="button" class="button"
							value="Back" onclick="window.location.href = 'http://localhost:8080/WebBanHang/AdminManagerCategoryServlet?command=DanhMuc'"><input type="hidden"
							value="insertCategory" name="command"></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="clear"></div>
		<jsp:include page="/templates/admin/inc/footer.jsp"></jsp:include>
</body>
</html>