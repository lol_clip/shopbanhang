<%@page import="model.bean.ListCategory"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Quản lý danh sách danh mục</title>
</head>
<body>
	<%
		ArrayList<ListCategory> alListCategory = (ArrayList<ListCategory>) request.getAttribute("alListCategory");
		String message = (String)request.getAttribute("message");
	%>
	<jsp:include page="/templates/admin/inc/header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="/templates/admin/inc/menuleft.jsp"></jsp:include>
		<div id="rightContent">
			<h3>Quản lý danh sách danh mục</h3>
			
			<%
				if(message!=null){
			%>
			<div class="sukses">Thông báo:<%=message %> </div>
			<%
				}
			%>
			<button><a href="<%=request.getContextPath()%>/AdminManagerCategoryListServlet?command=Them"><img src="<%=request.getContextPath() %>/templates/admin/images/add.png"> Thêm </a></button>
			<table class="data">
				<tr class="data">
					<th class="data" width="10%">STT</th>
					<th class="data">Mã danh sách danh mục</th>
					<th class="data">Tên danh sách danh mục</th>
					<th class="data" width="20%">Thao tác</th>
				</tr>
				<%
					if(alListCategory!=null){
						for(int i=0;i<alListCategory.size();i++){
				%>
				<tr class="data">
					<td class="data" width="30px"><%=i+1 %></td>
					<td class="data"><%=alListCategory.get(i).getMaDSDanhMuc()%></td>
					<td class="data"><%=alListCategory.get(i).getTenDSDanhMuc() %></td>
					<td class="data" width="75px">
						<center>
							<a href="<%=request.getContextPath()%>/AdminManagerCategoryListServlet?command=Sua&maMenuBar=<%=alListCategory.get(i).getMaDSDanhMuc()%>"><img src="<%=request.getContextPath() %>/templates/admin/images/update.png">&nbsp;&nbsp;Sửa</a>&nbsp;&nbsp;|&nbsp;&nbsp;
							<a href="<%=request.getContextPath()%>/AdminManagerCategoryListServlet?command=Xoa&maMenuBar=<%=alListCategory.get(i).getMaDSDanhMuc()%>"><img src="<%=request.getContextPath() %>/templates/admin/images/delete.png">&nbsp;&nbsp;Xóa</a>
						</center>
					</td>
				</tr>
				<%}} %>
			</table>
		</div>
		<div class="clear"></div>
		<jsp:include page="/templates/admin/inc/footer.jsp"></jsp:include>
</body>
</html>