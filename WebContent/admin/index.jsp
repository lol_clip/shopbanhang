<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Index</title>
</head>
<body>
	<%
		String message =(String) request.getAttribute("message");
	%>
	<jsp:include page="/templates/admin/inc/header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="/templates/admin/inc/menuleft.jsp"></jsp:include>
		<div id="rightContent">
			<h3>Dashboard</h3>
			<%
				if(message!=null){
			%>
			<div class="quoteOfDay">
				<b>Thông báo :</b><br> <i style="color: #5b5b5b;"><%=message %></i>
			</div>
			<%
				}else{
			%>
			<div class="quoteOfDay">
				<b>Quote of the day :</b><br> <i style="color: #5b5b5b;">"If
					you think you can, you really can"</i>
			</div>
			<%} %>
			<div class="shortcutHome">
				<a href=""><img
					src="<%=request.getContextPath()%>/templates/admin/images/posting.png"><br>Thông tin cập nhật</a>
			</div>
			<div class="shortcutHome">
				<a href="<%=request.getContextPath()%>/admin/upload.jsp"><img
					src="<%=request.getContextPath()%>/templates/admin/images/photo.png"><br>Upload
					Foto</a>
			</div>
			<div class="shortcutHome">
				<a href=""><img
					src="<%=request.getContextPath()%>/templates/admin/images/halaman.png"><br>Quản lý người dùng</a>
			</div>
			<div class="shortcutHome">
				<a href=""><img
					src="<%=request.getContextPath()%>/templates/admin/images/template.png"><br>Danh mục hình ảnh</a>
			</div>
			<div class="shortcutHome">
				<a href=""><img
					src="<%=request.getContextPath()%>/templates/admin/images/quote.png"><br>Ý tưởng sáng tạo</a>
			</div>
			<div class="shortcutHome">
				<a href=""><img
					src="<%=request.getContextPath()%>/templates/admin/images/bukutamu.png"><br>Send Email</a>
			</div>

			<div class="clear"></div>

			<div id="smallRight">
				<h3>Informasi web anda</h3>
				<table
					style="border: none; font-size: 12px; color: #5b5b5b; width: 100%; margin: 10px 0 10px 0;">
					<tr>
						<td style="border: none; padding: 4px;">Jumlah posting</td>
						<td style="border: none; padding: 4px;"><b>12</b></td>
					</tr>
					<tr>
						<td style="border: none; padding: 4px;">Jumlah kategori</td>
						<td style="border: none; padding: 4px;"><b>12</b></td>
					</tr>
					<tr>
						<td style="border: none; padding: 4px;">Jumlah komentar
							diterbitkan</td>
						<td style="border: none; padding: 4px;"><b>12</b></td>
					</tr>
					<tr>
						<td style="border: none; padding: 4px;">Jumlah komentar belum
							diterbitkan</td>
						<td style="border: none; padding: 4px;"><b>12</b></td>
					</tr>
					<tr>
						<td style="border: none; padding: 4px;">Jumlah foto dalam
							galeri</td>
						<td style="border: none; padding: 4px;"><b>12</b></td>
					</tr>
					<tr>
						<td style="border: none; padding: 4px;">Jumlah data buku tamu</td>
						<td style="border: none; padding: 4px;"><b>12</b></td>
					</tr>
				</table>
			</div>
			<div id="smallRight">
				<h3>Statistik pengunjung web</h3>

				<table
					style="border: none; font-size: 12px; color: #5b5b5b; width: 100%; margin: 10px 0 10px 0;">
					<tr>
						<td style="border: none; padding: 4px;">Pengunjung online</td>
						<td style="border: none; padding: 4px;"><b>12</b></td>
					</tr>
					<tr>
						<td style="border: none; padding: 4px;">Pengunjung hari ini</td>
						<td style="border: none; padding: 4px;"><b>12</b></td>
					</tr>
					<tr>
						<td style="border: none; padding: 4px;">Total pengunjung</td>
						<td style="border: none; padding: 4px;"><b>12</b></td>
					</tr>
					<tr>
						<td style="border: none; padding: 4px;">Hit hari ini</td>
						<td style="border: none; padding: 4px;"><b>12</b></td>
					</tr>
					<tr>
						<td style="border: none; padding: 4px;">Total hit</td>
						<td style="border: none; padding: 4px;"><b>12</b></td>
					</tr>
				</table>
			</div>
		</div>
		<div class="clear"></div>
		<jsp:include page="/templates/admin/inc/footer.jsp"></jsp:include>
</body>
</html>