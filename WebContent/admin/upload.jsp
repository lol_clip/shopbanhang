<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Upload file</title>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/templates/admin/css/mos-style.css">
</head>
<body>

	<div id="header">
		<div class="inHeaderLogin"></div>
	</div>

	<div id="loginForm">
		<div class="headLoginForm">Choose file to upload</div>
		<div class="fieldLogin">
			<center>
				<form method="post"
					action="<%=request.getContextPath()%>/UploadFileServlet"
					enctype="multipart/form-data">
					Select file to upload: <input type="file" name="uploadFile" /> <br />
					<br /> <input type="submit" class="button" value="Upload" />
				</form>
			</center>
		</div>
	</div>
</body>
</html>