<%@page import="model.bean.Category"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Quản lý danh mục</title>
</head>
<body>
	<%
		ArrayList<Category> alCategory = (ArrayList<Category>) request.getAttribute("alCategory");
		String message = (String)request.getAttribute("message");
	%>
	<jsp:include page="/templates/admin/inc/header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="/templates/admin/inc/menuleft.jsp"></jsp:include>
		<div id="rightContent">
			<h3>Quản lý danh mục</h3>
			<%
				if(message!=null){
			%>
			<div class="sukses">Thông báo:<%=message %> </div>
			<%
				}
			%>
			<button><a href="<%=request.getContextPath()%>/AdminManagerCategoryServlet?command=Them"><img src="<%=request.getContextPath() %>/templates/admin/images/add.png"> Thêm danh mục</a></button>
			<table class="data">
				<tr class="data">
					<th class="data" width="10%">STT</th>
					<th class="data">Mã danh mục</th>
					<th class="data">Tên danh mục</th>
					<th class="data">Mã danh sách danh mục</th>
					<th class="data" width="20%">Thao tác</th>
				</tr>
				<%
					if(alCategory!=null){
						for(int i=0; i<alCategory.size();i++){
				%>
				<tr class="data">
					<td class="data" width="10%"><%=i+1 %></td>
					<td class="data" width="20%"><%=alCategory.get(i).getMaDanhMuc() %></td>
					<td class="data"><%=alCategory.get(i).getTenDanhMuc() %></td>
					<td class="data" width="25%"><%=alCategory.get(i).getMaDSDanhMuc() %></td>
					<td class="data" width="20%">
						<center>
							<a href="<%=request.getContextPath()%>/AdminManagerCategoryServlet?command=Sua&maDanhMuc=<%=alCategory.get(i).getMaDanhMuc()%>"><img src="<%=request.getContextPath() %>/templates/admin/images/update.png">&nbsp;&nbsp;Sửa</a>&nbsp;&nbsp;|&nbsp;&nbsp;
							<a href="<%=request.getContextPath()%>/AdminManagerCategoryServlet?command=Xoa&maDanhMuc=<%=alCategory.get(i).getMaDanhMuc()%>"><img src="<%=request.getContextPath() %>/templates/admin/images/delete.png">&nbsp;&nbsp;Xóa</a>
						</center>
					</td>
				</tr>
				<%
					}}
				%>
				
				
			</table>
		</div>
		<div class="clear"></div>
		<jsp:include page="/templates/admin/inc/footer.jsp"></jsp:include>
</body>
</html>