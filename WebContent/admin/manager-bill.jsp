<%@page import="model.bean.Bill"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Quản lý hóa đơn</title>
</head>
<body>
	<%
		ArrayList<Bill> alBill = (ArrayList<Bill>) request.getAttribute("alBill");
		String message = (String)request.getAttribute("message");
	%>
	<jsp:include page="/templates/admin/inc/header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="/templates/admin/inc/menuleft.jsp"></jsp:include>
		<div id="rightContent">
			<h3>Quản lý hóa đơn</h3>
			<%
				if(message!=null){
			%>
			<div class="sukses">Thông báo:<%=message %> </div>
			<%
				}
			%>
			<table class="data">
				<tr class="data">
					<th class="data" width="5%">STT</th>
					<th class="data" width="13%">Mã hóa đơn</th>
					<th class="data" width="15%">Tên thành viên</th>
					<th class="data" width="10%">Tổng tiền</th>
					<th class="data" width="10%">Hình thức thanh toán</th>
					<th class="data">Địa chỉ email</th>
					<th class="data" width="11%">Thời gian</th>
					<th class="data" width="10%">Thao tác</th>
				</tr>
				<%
					if (alBill != null) {
						for (int i = 0; i < alBill.size(); i++) {
				%>
				<tr class="data">
					<td class="data" width="30px"><%=i+1 %></td>
					<td class="data"><%=alBill.get(i).getMaHoaDon() %></td>
					<td class="data"><%=alBill.get(i).getTenThanhVien() %></td>
					<td class="data"><%=alBill.get(i).getTongTien() %></td>
					<td class="data"><%=alBill.get(i).getHinhThucThanhToan() %></td>
					<td class="data"><%=alBill.get(i).getDiaChi() %></td>
					<td class="data"><%=alBill.get(i).getThoiGian() %></td>
					<td class="data" width="75px">
						<center>
							<a href="<%=request.getContextPath()%>/AdminManagerBillServlet?command=Xem&maHoaDon=<%=alBill.get(i).getMaHoaDon()%>">Xem</a>&nbsp;|
							<a href="<%=request.getContextPath()%>/AdminManagerBillServlet?command=Xoa&maHoaDon=<%=alBill.get(i).getMaHoaDon()%>">Xóa</a>
						</center>
					</td>
				</tr>
				<%
					}
					}
				%>
			</table>
		</div>
		<div class="clear"></div>
		<jsp:include page="/templates/admin/inc/footer.jsp"></jsp:include>
</body>
</html>