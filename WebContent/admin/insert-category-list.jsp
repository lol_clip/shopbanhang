<%@page import="model.bean.ListCategory"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert category</title>
</head>
<body>
	<jsp:include page="/templates/admin/inc/header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="/templates/admin/inc/menuleft.jsp"></jsp:include>
		<div id="rightContent">
			<h3>Thêm menubar mới</h3>
			<form action="AdminManagerCategoryListServlet" method="post">
				<table width="95%">
					<tr>
						<td><b>Tên menubar</b></td>
						<td><input type="text" class="sedang" name="tenMenuBar" required></td>
					</tr>

					<tr>
						<td></td>
						<td><input type="submit" class="button" value="Add"
							name="them"> <input type="reset" class="button"
							value="Reset"> <input type="button" class="button"
							value="Back" onclick="window.location.href = 'http://localhost:8080/WebBanHang/AdminManagerCategoryListServlet?command=DanhMuc'"><input type="hidden"
							value="insertCategoryList" name="command"></td>
					</tr>
				</table>
			</form>
		</div>
		<div class="clear"></div>
		<jsp:include page="/templates/admin/inc/footer.jsp"></jsp:include>
</body>
</html>