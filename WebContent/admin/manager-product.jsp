<%@page import="model.bean.Product"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Quản lý sản phẩm</title>
</head>
<body>
	<%
		ArrayList<Product> alProduct = (ArrayList<Product>) request.getAttribute("alProduct");
		String message = (String) request.getAttribute("message");
	%>
	<jsp:include page="/templates/admin/inc/header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="/templates/admin/inc/menuleft.jsp"></jsp:include>
		<div id="rightContent">
			<h3>Quản lý sản phẩm</h3>
			<%
				if (message != null) {
			%>
			<div class="sukses">
				Thông báo:<%=message%>
			</div>
			<%
				}
			%>
			<button>
				<a
					href="<%=request.getContextPath()%>/AdminManagerProductServlet?command=Them"><img
					alt="Thêm"
					src="<%=request.getContextPath()%>/templates/admin/images/add.png">Thêm</a>
			</button>
			<table class="data">
				<tr class="data">
					<th class="data" width="5%">STT</th>
					<th class="data" width="15%">Mã sản phẩm</th>
					<th class="data" width="15%">Tên sản phẩm</th>
					<th class="data" width="10%">Hình ảnh</th>
					<th class="data" width="10%">Giá</th>
					<th class="data">Mô tả</th>
					<th class="data" width="10%">Mã thể loại</th>
					<th class="data" width="10%">Thao tác</th>
				</tr>
				<%
					if (alProduct != null) {
						for (int i = 0; i < alProduct.size(); i++) {
				%>
				<tr class="data">
					<td class="data" width="30px"><%=i + 1%></td>
					<td class="data"><%=alProduct.get(i).getMaSanPham()%></td>
					<td class="data"><%=alProduct.get(i).getTenSanPham()%></td>
					<td class="data">
						<a href="<%=request.getContextPath()%>/admin/imageDetail.jsp?image=<%=alProduct.get(i).getHinhAnh()%>" target="_blank"><%=alProduct.get(i).getHinhAnh()%></a>
					</td>
					<td class="data"><%=alProduct.get(i).getGiaSanPham()%></td>
					<td class="data"><%=alProduct.get(i).getMoTa()%></td>
					<td class="data"><%=alProduct.get(i).getmaDanhMuc()%></td>
					<td class="data" width="75px">
						<center>
							<a
								href="<%=request.getContextPath()%>/AdminManagerProductServlet?command=Sua&maSanPham=<%=alProduct.get(i).getMaSanPham()%>">Sửa</a>&nbsp;|
							<a
								href="<%=request.getContextPath()%>/AdminManagerProductServlet?command=Xoa&maSanPham=<%=alProduct.get(i).getMaSanPham()%>">Xóa</a>
						</center>
					</td>
				</tr>
				<%
					}
					}
				%>
			</table>
		</div>
		<div class="clear"></div>
		<jsp:include page="/templates/admin/inc/footer.jsp"></jsp:include>
</body>
</html>