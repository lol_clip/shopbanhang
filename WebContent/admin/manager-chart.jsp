<%@page import="model.bean.Value"%>
<%@page import="model.bean.Category"%>
<%@page import="java.util.ArrayList"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Quản lý thống kê</title>
<%
	ArrayList<Value> alValue = (ArrayList<Value>) request.getAttribute("alValue");
%>
<link rel="stylesheet" type="text/css" href="${root }/templates/admin/css/mos-style.css">
<!-- load Google AJAX API -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
	// Load the Visualization API and the piechart package.
	google.load('visualization', '1', {
		'packages' : [ 'columnchart' ]
	});

	// Set a callback to run when the Google Visualization API is loaded.
	google.setOnLoadCallback(drawChart);

	// Callback that creates and populates a data table,
	// instantiates the pie chart, passes in the data and
	// draws it.
	function drawChart() {

		// Create the data table.    
		var data = google.visualization.arrayToDataTable([
				[ 'Country', 'Area(square km)' ], 
				//<c:forEach var="item" items="${alValue}">['$(item.name)',$(item.value)],</c:forEach>
				['hello',5],['hellos',5],
		 ]);
		// Set chart options
		var options = {
			'title' : 'Java Technologies for Web Applications',
			is3D : true,
			pieSliceText : 'label',
			tooltip : {
				showColorCode : true
			},
			'width' : 700,
			'height' : 300
		};

		// Instantiate and draw our chart, passing in some options.
		var chart = new google.visualization.PieChart(document
				.getElementById('Chart2'));
		chart.draw(data, options);
	}
</script>
<!-- load Google AJAX API -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
	//load the Google Visualization API and the chart
	google.load('visualization', '1', {
		'packages' : [ 'columnchart' ]
	});

	//set callback
	google.setOnLoadCallback(createChart);

	//callback function
	function createChart() {

		//create data table object
		var dataTable = new google.visualization.DataTable();

		//define columns
		dataTable.addColumn('string', 'Quarters 2009');
		dataTable.addColumn('number', 'Technologies');

		//define rows of data
		dataTable.addRows([ [ 'JSP & Servlet', 20 ], [ 'Spring', 12 ],
				[ 'Struts', 7 ] ]);

		//instantiate our chart object
		var chart = new google.visualization.ColumnChart(document
				.getElementById('chart'));

		//define options for visualization
		var options = {
			width : 700,
			height : 300,
			is3D : true,
			title : 'Java Technologies for Web Applications'
		};

		//draw our chart
		chart.draw(dataTable, options);

	}
</script>
</head>
<body>

	<jsp:include page="/templates/admin/inc/header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="/templates/admin/inc/menuleft.jsp"></jsp:include>
		<div id="rightContent">
			<h3>Biểu đồ thống kê</h3>
			<h2>Biểu đồ tròn</h2>
			<div id="Chart2"></div><br><br>
			<center>Hình 1: biểu đồ tròn thống kê sản phẩm</center>
			<h2>Biểu đồ cột</h2>
			<div id="chart"></div>
			<center>Hình 2: biểu đồ cột thống kê sản phẩm</center>
			<table class="data">

			</table>
		</div>
		<div class="clear"></div>
		<jsp:include page="/templates/admin/inc/footer.jsp"></jsp:include>
</body>
</html>