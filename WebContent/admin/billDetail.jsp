<%@page import="model.bean.BillDetail"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Bill Detail</title>
</head>
<body>
	<%
		ArrayList<BillDetail> alBillDetail = (ArrayList<BillDetail>) request.getAttribute("alBillDetail");
	%>
	<jsp:include page="/templates/admin/inc/header.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="/templates/admin/inc/menuleft.jsp"></jsp:include>
		<div id="rightContent">
			<h3>Chi tiết hóa đơn</h3>

			<table class="data">
				<tr class="data">
					<th class="data" width="10%">STT</th>
					<th class="data" width="20%">Mã chi tiêt hóa đơn</th>
					<th class="data" >Mã hóa đơn</th>
					<th class="data" width="15%">Mã sản phẩm</th>
					<th class="data" width="15%">Giá</th>
					<th class="data" width="15%">Số lượng</th>
				</tr>
				<%
					if (alBillDetail != null) {
						for (int i = 0; i < alBillDetail.size(); i++) {
				%>
				<tr class="data">
					<td class="data" width="30px"><%=i+1 %></td>
					<td class="data"><%=alBillDetail.get(i).getMaChiTietHoaDon() %></td>
					<td class="data"><%=alBillDetail.get(i).getMaHoaDon() %></td>
					<td class="data"><%=alBillDetail.get(i).getMaSanPham() %></td>
					<td class="data"><%=alBillDetail.get(i).getGia()%></td>
					<td class="data"><%=alBillDetail.get(i).getSoLuong() %></td>
				</tr>
				<%
					}
					}
				%>
			</table>
			<a href="<%=request.getContextPath() %>/AdminManagerBillServlet?command=Back"><img alt="Back" src="<%=request.getContextPath()%>/templates/admin/images/go-back.png">Back</a>
		</div>
		<div class="clear"></div>
		<jsp:include page="/templates/admin/inc/footer.jsp"></jsp:include>
</body>
</html>