<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>
</head>
<body>
	<jsp:include page="/templates/public/inc/header.jsp"></jsp:include>
	<div class="container">
		<div class="account">
			<h2 class="account-in">Login</h2>
			<form action="UserControllerServlet" method="post">
				<%
					if(request.getAttribute("error")!=null){
				%>	
				<div>
					<p style= "color:red"><%=request.getAttribute("error")%></p>
				</div>
				<% 
					}
				%>
				<div>
					<span class="name-in">User name*</span> 
					<input type="text" name="username" required>
					<span id="user-result"></span>
				</div>
				<div>
					<span class="word">Password*</span> 
					<input type="password" name="password" required>
					<span id=""></span>
				</div>
				<input type="hidden" value="LoginUser" name="command"> 
				<input type="submit" value="Login">
			</form>
		</div>
	</div>
	<jsp:include page="/templates/public/inc/footer.jsp"></jsp:include>
</body>
</html>