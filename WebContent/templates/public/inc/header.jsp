<%@page import="model.bean.Item"%>
<%@page import="java.util.Map"%>
<%@page import="model.bean.Cart"%>
<%@page import="model.bo.CategoryBO"%>
<%@page import="model.bo.ListCategoryBO"%>
<%@page import="model.bean.Category"%>
<%@page import="model.bean.ListCategory"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Header</title>
<link
	href="<%=request.getContextPath()%>/templates/public/css/bootstrap.css"
	rel="stylesheet" type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script
	src="<%=request.getContextPath()%>/templates/public/js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link
	href="<%=request.getContextPath()%>/templates/public/css/style.css"
	rel="stylesheet" type="text/css" media="all" />
<!--//theme-style-->
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<!--fonts-->
<link
	href='http://fonts.googleapis.com/css?family=Exo:100,200,300,400,500,600,700,800,900'
	rel='stylesheet' type='text/css'>
<!--//fonts-->
<script type="text/javascript"
	src="<%=request.getContextPath()%>/templates/public/js/move-top.js"></script>
<script type="text/javascript"
	src="<%=request.getContextPath()%>/templates/public/js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event) {
			event.preventDefault();
			$('html,body').animate({
				scrollTop : $(this.hash).offset().top
			}, 1000);
		});
	});
</script>
<!--slider-script-->
<script
	src="<%=request.getContextPath()%>/templates/public/js/responsiveslides.min.js"></script>
<script>
	$(function() {
		$("#slider1").responsiveSlides({
			auto : true,
			speed : 500,
			namespace : "callbacks",
			pager : true,
		});
	});
</script>
<!--//slider-script-->

<div id="fb-root"></div>
<script>
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id))
			return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=390106584447051";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
</head>
<body>
	<%
		ListCategoryBO listCategoryBO = new ListCategoryBO();
		ArrayList<ListCategory> alListCategory = listCategoryBO.getListMenuBar();
		CategoryBO categoryBO = new CategoryBO();
		ArrayList<Category> alCategory = categoryBO.getListCategory();
		String user = null;
		if (session.getAttribute("user") != null) {
			user = (String) session.getAttribute("user");
		}
		Cart cart = (Cart) session.getAttribute("cart");
		if (cart == null) {
			cart = new Cart();
			session.setAttribute("cart", cart);
		}
	%>

	<!--header-->
	<div class="header">
		<div class="header-top">
			<div class="container">
				<div class="header-top-in">
					<div class="logo">
						<a href="<%=request.getContextPath()%>/IndexServlet"><img
							src="<%=request.getContextPath()%>/templates/public/images/logo.png"
							alt=" "></a>
					</div>
					<div class="header-in">
						<ul class="icon1 sub-icon1">
							<li><a href="<%=request.getContextPath()%>/WishListServlet">WISH
									LIST (0)</a></li>
							<li><a href="#"> SHOPPING CART</a></li>
							<li><a href="<%=request.getContextPath()%>/CheckOutServlet?command=null">CHECKOUT</a></li>
							<li><div class="cart">
									<a href="#" class="cart-in"> </a> <span><%=cart.countItem()%></span>
								</div>
								<ul class="sub-icon1 list">
									<h3>
										Recently added items(<%=cart.countItem()%>)
									</h3>
									<div class="shopping_cart">
										<%
											for (Map.Entry<Integer, Item> list : cart.getCartItem().entrySet()) {
										%>
										<div class="cart_box">
											<div class="message">
												<div class="alert-close"></div>
												<div class="list_img">
													<img
														src="<%=request.getContextPath()%>/templates/public/images/<%=list.getValue().getSanPham().getHinhAnh()%>"
														class="img-responsive" alt="">
												</div>
												<div class="list_desc">
													<h4>
														<a
															href="<%=request.getContextPath()%>/CartServlet?command=remove&maSanPham=<%=list.getValue().getSanPham().getMaSanPham()%>"><%=list.getValue().getSanPham().getTenSanPham()%></a>
													</h4>
													<%=list.getValue().getSoLuong()%>
													x<span class="actual"> $<%=list.getValue().getSanPham().getGiaSanPham()%></span>
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
										<%
											}
										%>
									</div>
									<div class="total">
										<div class="total_left">Cart Subtotal :</div>
										<div class="total_right">
											$<%=cart.totalCart()%></div>
										<div class="clearfix"></div>
									</div>
									<div class="login_buttons">
										<div class="check_button">
											<a href="<%=request.getContextPath()%>/CheckOutServlet?command=null">Check
												out</a>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="clearfix"></div>
								</ul></li>
						</ul>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="header-bottom">
			<div class="container">
				<div class="h_menu4">
					<a class="toggleMenu" href="#">Menu</a>
					<ul class="nav">
						<li class="active"><a
							href="<%=request.getContextPath()%>/IndexServlet"><i> </i>Home</a></li>
						<%
							if (alListCategory != null) {
								for (ListCategory listCategory : alListCategory) {
						%>
						<li><a href=""><%=listCategory.getTenDSDanhMuc()%></a>
							<ul class="drop">
								<%
									if (alCategory != null) {
												for (Category category : alCategory) {
													if (category.getMaDSDanhMuc() == listCategory.getMaDSDanhMuc()) {
								%>
								<li><a
									href="<%=request.getContextPath()%>/ProductByCategoryServlet?maDanhMuc=<%=category.getMaDanhMuc()%>&pages=1"><%=category.getTenDanhMuc()%></a></li>
								<%
									}
												}
											}
								%>
							</ul></li>
						<%
							}
							}
						%>
						<li><a href="">Contact</a></li>
						<li><a href="">Aboutme</a></li>
					</ul>
					<script type="text/javascript"
						src="<%=request.getContextPath()%>/templates/public/js/nav.js"></script>
				</div>
			</div>
		</div>
		<div class="header-bottom-in">
			<div class="container">
				<div class="header-bottom-on">
					<p class="wel">
						<%
							if (user == null) {
						%>
						Welcome visitor you can <a
							href="<%=request.getContextPath()%>/UserControllerServlet?command=Login">login</a>
						or <a
							href="<%=request.getContextPath()%>/UserControllerServlet?command=Register">create</a>
						an account.
						<%
							} else {
						%>
						Welcome <a href="<%=request.getContextPath()%>/IndexServlet"><%=user%></a>
						, you can logout <a href="<%=request.getContextPath()%>/IndexServlet">here</a>
						<%
							}
						%>
					</p>
					<div class="header-can">
						<ul class="social-in">
							<li><a href="https://www.facebook.com/longpro.hack" target="_blank"><i> </i></a></li>
							<li><a href="https://www.facebook.com/longpro.hack" target="_blank"><i class="facebook"> </i></a></li>
							<li><a href="https://www.facebook.com/longpro.hack" target="_blank"><i class="twitter"> </i></a></li>
							<li><a href="https://www.facebook.com/longpro.hack" target="_blank"><i class="skype"> </i></a></li>
						</ul>
						<div class="down-top">
							<select class="in-drop" name="currency">
								<option value="Dollars" class="in-of" >Dollars</option>
								<option value="VND" class="in-of">VNĐ</option>
							</select>
						</div>
						<div class="search">
							<form action="SearchServlet" method="post">
								<input type="text" value="Search" onfocus="this.value = '';"
									onblur="if (this.value == '') {this.value = '';}"> <input
									type="submit" value="">
							</form>
						</div>

						<div class="clearfix"></div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>