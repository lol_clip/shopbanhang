<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Header</title>
	<link rel="stylesheet" type="text/css" href="<%=request.getContextPath() %>/templates/admin/css/mos-style.css">
	
</head>
<body>
	<div id="header">
	<div class="inHeader">
		<div class="mosAdmin">
		Chào, <%=session.getAttribute("admin") %><br>
		<a href="<%=request.getContextPath()%>/IndexServlet" target="_blank">Webbanhang</a> | <a href="#">Help</a> | <a href="<%=request.getContextPath() %>/AdminLoginServlet?command=Logout">Logout</a>
		</div>
	<div class="clear"></div>
	</div>
</div>
</body>
</html>