<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Menu</title>
</head>
<body>
	<div id="leftBar">
	<ul>
		<li><a href="<%=request.getContextPath() %>/AdminIndexServlet">Trang chủ</a></li>
		<li><a href="<%=request.getContextPath() %>/AdminManagerCategoryListServlet?command=Menubar">Menubar</a></li>
		<li><a href="<%=request.getContextPath() %>/AdminManagerCategoryServlet?command=DanhMuc">Danh mục</a></li>
		<li><a href="<%=request.getContextPath() %>/AdminManagerProductServlet?command=SanPham">Sản phẩm</a></li>
		<li><a href="<%=request.getContextPath() %>/AdminManagerBillServlet?command=HoaDon">Hóa đơn</a></li>
		<li><a href="<%=request.getContextPath() %>/AdminManagerChartServlet?command=ThongKe">Thống kê</a></li>
	</ul>
	</div>
</body>
</html>