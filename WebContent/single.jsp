<%@page import="model.bean.Product"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Single</title>
</head>
<body>
	<%
		Product product = (Product)request.getAttribute("product");
	%>
	
	<jsp:include page="/templates/public/inc/header.jsp"></jsp:include>
	<link rel="stylesheet"
		href="<%=request.getContextPath()%>/templates/public/css/etalage.css">
	<script
		src="<%=request.getContextPath()%>/templates/public/js/jquery.etalage.min.js"></script>
	<script>
		jQuery(document)
				.ready(
						function($) {

							$('#etalage')
									.etalage(
											{
												thumb_image_width : 300,
												thumb_image_height : 400,
												source_image_width : 900,
												source_image_height : 1200,
												show_hint : true,
												click_callback : function(
														image_anchor,
														instance_id) {
													alert('Callback example:\nYou clicked on an image with the anchor: "'
															+ image_anchor
															+ '"\n(in Etalage instance: "'
															+ instance_id
															+ '")');
												}
											});

						});
	</script>

	<div class="container">
		<div class="single">
			<div class="col-md-9 top-in-single">
				<div class="col-md-5 single-top">
					<ul id="etalage">
						<li><a href="optionallink.html"> <img
								class="etalage_thumb_image img-responsive"
								src="<%=request.getContextPath()%>/templates/public/images/si1.jpg"
								alt=""> <img class="etalage_source_image img-responsive"
								src="<%=request.getContextPath()%>/templates/public/images/si1.jpg"
								alt="">
						</a></li>
						<li><img class="etalage_thumb_image img-responsive"
							src="<%=request.getContextPath()%>/templates/public/images/si2.jpg"
							alt=""> <img class="etalage_source_image img-responsive"
							src="<%=request.getContextPath()%>/templates/public/images/si2.jpg"
							alt=""></li>
						<li><img class="etalage_thumb_image img-responsive"
							src="<%=request.getContextPath()%>/templates/public/images/si.jpg"
							alt=""> <img class="etalage_source_image img-responsive"
							src="<%=request.getContextPath()%>/templates/public/images/si.jpg"
							alt=""></li>
						<li><img class="etalage_thumb_image img-responsive"
							src="<%=request.getContextPath()%>/templates/public/images/si1.jpg"
							alt=""> <img class="etalage_source_image img-responsive"
							src="<%=request.getContextPath()%>/templates/public/images/si1.jpg"
							alt=""></li>
					</ul>

				</div>
				<div class="col-md-7 single-top-in">
					<div class="single-para">
						<h4><%=product.getTenSanPham() %></h4>
						<div class="para-grid">
							<span class="add-to">$<%=product.getGiaSanPham() %></span> <a href="#"
								class="hvr-shutter-in-vertical cart-to">Add to Cart</a>
							<div class="clearfix"></div>
						</div>
						<h5>100 items in stock</h5>
						<div class="available">
							<h6>Available Options :</h6>
							<ul>
								<li>Color: <select>
										<option>Silver</option>
										<option>Black</option>
										<option>Dark Black</option>
										<option>Red</option>
								</select></li>
								<li>Size:<select>
										<option>Large</option>
										<option>Medium</option>
										<option>small</option>
										<option>Large</option>
										<option>small</option>
								</select></li>
								<li>Quality:<select>
										<option>1</option>
										<option>2</option>
										<option>3</option>
										<option>4</option>
										<option>5</option>
								</select></li>
							</ul>
						</div>
						<p><%=product.getMoTa() %></p>

						<a href="#" class="hvr-shutter-in-vertical ">More details</a>

					</div>
				</div>
				<div class="clearfix"></div>
				<div class="content-top-in">
					<div class="col-md-4 top-single">
						<div class="col-md">
							<img
								src="<%=request.getContextPath()%>/templates/public/images/pic8.jpg"
								alt="" />
							<div class="top-content">
								<h5>Mascot Kitty - White</h5>
								<div class="white">
									<a href="#"
										class="hvr-shutter-in-vertical hvr-shutter-in-vertical2">ADD
										TO CART</a>
									<p class="dollar">
										<span class="in-dollar">$</span><span>2</span><span>0</span>
									</p>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 top-single">
						<div class="col-md">
							<img
								src="<%=request.getContextPath()%>/templates/public/images/pic9.jpg"
								alt="" />
							<div class="top-content">
								<h5>Mascot Kitty - White</h5>
								<div class="white">
									<a href="#"
										class="hvr-shutter-in-vertical hvr-shutter-in-vertical2">ADD
										TO CART</a>
									<p class="dollar">
										<span class="in-dollar">$</span><span>2</span><span>0</span>
									</p>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 top-single-in">
						<div class="col-md">
							<img
								src="<%=request.getContextPath()%>/templates/public/images/pic10.jpg"
								alt="" />
							<div class="top-content">
								<h5>Mascot Kitty - White</h5>
								<div class="white">
									<a href="#"
										class="hvr-shutter-in-vertical hvr-shutter-in-vertical2">ADD
										TO CART</a>
									<p class="dollar">
										<span class="in-dollar">$</span><span>2</span><span>0</span>
									</p>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>

					<div class="clearfix"></div>
				</div>
			</div>
			
			<div class="col-md-3">
				<div class="single-bottom">
					<h4>Categories</h4>
					<ul>
						<li><a href="#"><i> </i>Fusce feugiat</a></li>
						<li><a href="#"><i> </i>Mascot Kitty</a></li>
						<li><a href="#"><i> </i>Fusce feugiat</a></li>
						<li><a href="#"><i> </i>Mascot Kitty</a></li>
						<li><a href="#"><i> </i>Fusce feugiat</a></li>
					</ul>
				</div>
				<div class="single-bottom">
					<h4>Product Categories</h4>
					<ul>
						<li><a href="#"><i> </i>feugiat(5)</a></li>
						<li><a href="#"><i> </i>Fusce (4)</a></li>
						<li><a href="#"><i> </i> feugiat (4)</a></li>
						<li><a href="#"><i> </i>Fusce (4)</a></li>
						<li><a href="#"><i> </i> feugiat(2)</a></li>
					</ul>
				</div>
				<div class="single-bottom">
					<h4>Product Categories</h4>
					<div class="product">
						<img class="img-responsive fashion"
							src="<%=request.getContextPath()%>/templates/public/images/st1.jpg"
							alt="">
						<div class="grid-product">
							<a href="#" class="elit">Consectetuer adipiscing elit</a> <span
								class="price price-in"><small>$500.00</small> $400.00</span>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="product">
						<img class="img-responsive fashion"
							src="<%=request.getContextPath()%>/templates/public/images/st2.jpg"
							alt="">
						<div class="grid-product">
							<a href="#" class="elit">Consectetuer adipiscing elit</a> <span
								class="price price-in"><small>$500.00</small> $400.00</span>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="product">
						<img class="img-responsive fashion"
							src="<%=request.getContextPath()%>/templates/public/images/st3.jpg"
							alt="">
						<div class="grid-product">
							<a href="#" class="elit">Consectetuer adipiscing elit</a> <span
								class="price price-in"><small>$500.00</small> $400.00</span>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<div>
				<div class="fb-comments" data-href="http://localhost:8080/WebBanHang/SingleByProductServlet?maSanPham=<%=product.getMaSanPham()%>" data-width="850" data-numposts="5"></div>
			</div>
		</div>
	</div>
	<jsp:include page="/templates/public/inc/footer.jsp"></jsp:include>
</body>
</html>