<%@page import="java.util.ArrayList"%>
<%@page import="model.bean.Product"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Index</title>
</head>
<body>
	<%
		Product product = (Product) request.getAttribute("product");
		ArrayList<Product> alFeature = (ArrayList<Product>) request.getAttribute("alFeature");
		ArrayList<Product> alLatest = (ArrayList<Product>) request.getAttribute("alLatest");
	%>
	<jsp:include page="/templates/public/inc/header.jsp"></jsp:include>
	<div class="banner-mat">
		<div class="container">
			<div class="banner">

				<!-- Slideshow 4 -->
				<div class="slider">
					<ul class="rslides" id="slider1">
						<li><img
							src="<%=request.getContextPath()%>/templates/public/images/banner.jpg"
							alt=""></li>
						<li><img
							src="<%=request.getContextPath()%>/templates/public/images/banner1.jpg"
							alt=""></li>
						<li><img
							src="<%=request.getContextPath()%>/templates/public/images/banner.jpg"
							alt=""></li>
						<li><img
							src="<%=request.getContextPath()%>/templates/public/images/banner2.jpg"
							alt=""></li>
					</ul>
				</div>

				<div class="banner-bottom">
					<div class="banner-matter">
						<p>
							HOT -
							<%=product.getTenSanPham()%>
							for just $<%=product.getGiaSanPham()%></p>
						<a
							href="<%=request.getContextPath()%>/SingleByProductServlet?maSanPham=<%=product.getMaSanPham()%>"
							class="hvr-shutter-in-vertical ">Purchase</a>
					</div>
					<div class="purchase">
						<a
							href="<%=request.getContextPath()%>/SingleByProductServlet?maSanPham=<%=product.getMaSanPham()%>"
							class="hvr-shutter-in-vertical hvr-shutter-in-vertical2 ">Purchase</a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<!-- //slider-->
		</div>
	</div>
	<!---->
	<div class="container">
		<div class="content">
			<div class="content-top">
				<h3 class="future">FEATURED</h3>
				<div class="content-top-in">
					<%
						if (alFeature != null) {
							for (Product product2 : alFeature) {
					%>
					<div class="col-md-3 md-col" style="padding-top: 20px">
						<div class="col-md">
							<a
								href="<%=request.getContextPath()%>/SingleByProductServlet?maSanPham=<%=product2.getMaSanPham()%>"><img
								src="<%=request.getContextPath()%>/templates/public/images/<%=product2.getHinhAnh()%>"
								alt="" /></a>
							<div class="top-content">
								<h5>
									<a
										href="<%=request.getContextPath()%>/SingleByProductServlet?maSanPham=<%=product2.getMaSanPham()%>"><%=product2.getTenSanPham()%></a>
								</h5>
								<div class="white">
									<a
										href="<%=request.getContextPath()%>/CartServlet?command=plus&maSanPham=<%=product2.getMaSanPham()%>"
										class="hvr-shutter-in-vertical hvr-shutter-in-vertical2 ">ADD
										TO CART</a>
									<p class="dollar">
										<span class="in-dollar">$</span><span><%=product2.getGiaSanPham()%></span>
									</p>
									<div class="clearfix"></div>
								</div>

							</div>
						</div>
					</div>
					<%
						}
						}
					%>

					<div class="clearfix"></div>
				</div>
			</div>
			<!---->
			<div class="content-middle">
				<h3 class="future">BRANDS</h3>
				<div class="content-middle-in">
					<ul id="flexiselDemo1">
						<li><img
							src="<%=request.getContextPath()%>/templates/public/images/ap.png" /></li>
						<li><img
							src="<%=request.getContextPath()%>/templates/public/images/ap1.png" /></li>
						<li><img
							src="<%=request.getContextPath()%>/templates/public/images/ap2.png" /></li>
						<li><img
							src="<%=request.getContextPath()%>/templates/public/images/ap3.png" /></li>

					</ul>
					<script type="text/javascript">
						$(window).load(function() {
							$("#flexiselDemo1").flexisel({
								visibleItems : 4,
								animationSpeed : 1000,
								autoPlay : true,
								autoPlaySpeed : 3000,
								pauseOnHover : true,
								enableResponsiveBreakpoints : true,
								responsiveBreakpoints : {
									portrait : {
										changePoint : 480,
										visibleItems : 1
									},
									landscape : {
										changePoint : 640,
										visibleItems : 2
									},
									tablet : {
										changePoint : 768,
										visibleItems : 3
									}
								}
							});

						});
					</script>
					<script type="text/javascript"
						src="<%=request.getContextPath()%>/templates/public/js/jquery.flexisel.js"></script>

				</div>
			</div>
			<!---->
			<div class="content-bottom">
				<h3 class="future">LATEST</h3>
				<div class="content-bottom-in">
					<ul id="flexiselDemo2">
						<%
							if (alLatest != null) {
								for (Product product3 : alLatest) {
						%>
						<li><div class="col-md men" style="padding-bottom:50px">
								<a
									href="<%=request.getContextPath()%>/SingleByProductServlet?maSanPham=<%=product3.getMaSanPham()%>"
									class="compare-in "><img
									src="<%=request.getContextPath()%>/templates/public/images/<%=product3.getHinhAnh()%>"
									alt="" />
									<div class="compare in-compare">
										<span>Add to Compare</span> <span>Add to Whislist</span>
									</div></a>
								<div class="top-content bag">
									<h5>
										<a
											href="<%=request.getContextPath()%>/SingleByProductServlet?maSanPham=<%=product3.getMaSanPham()%>"><%=product3.getTenSanPham()%></a>
									</h5>
									<div class="white">
										<a
											href="<%=request.getContextPath()%>/CartServlet?command=plus&maSanPham=<%=product3.getMaSanPham()%>"
											class="hvr-shutter-in-vertical hvr-shutter-in-vertical2">ADD
											TO CART</a>
										<p class="dollar">
											<span class="in-dollar">$</span><span><%=product3.getGiaSanPham()%></span>
										</p>
										<div class="clearfix"></div>
									</div>
								</div>
							</div></li>
						<%
							}
							}
						%>

					</ul>
					<script type="text/javascript">
						$(window).load(function() {
							$("#flexiselDemo2").flexisel({
								visibleItems : 4,
								animationSpeed : 1000,
								autoPlay : true,
								autoPlaySpeed : 3000,
								pauseOnHover : true,
								enableResponsiveBreakpoints : true,
								responsiveBreakpoints : {
									portrait : {
										changePoint : 480,
										visibleItems : 1
									},
									landscape : {
										changePoint : 640,
										visibleItems : 2
									},
									tablet : {
										changePoint : 768,
										visibleItems : 3
									}
								}
							});

						});
					</script>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="/templates/public/inc/footer.jsp"></jsp:include>
</body>
</html>