<%@page import="model.bo.ProductBO"%>
<%@page import="model.bean.Cart"%>
<%@page import="model.bean.Product"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Products</title>
</head>
<body>
	<%
		ArrayList<Product> alProduct = (ArrayList<Product>)request.getAttribute("alProduct");
		ArrayList<Product> alProductByPage = (ArrayList<Product>)request.getAttribute("alProductByPage");
		int pages = (int)request.getAttribute("pages");
		Cart cart = (Cart) session.getAttribute("cart");
		int maDanhMuc = (int)request.getAttribute("maDanhMuc");
		int total = (int)request.getAttribute("total");
		if(cart==null){
			cart = new Cart();
			session.setAttribute("cart", cart);
		}
		
	%>
	<jsp:include page="/templates/public/inc/header.jsp"></jsp:include>
	<div class="container">
		<div class="products">
			<h2 class=" products-in">PRODUCTS</h2>
			<%
					if(alProductByPage!=null){
						for(int num=0;num<alProductByPage.size();num++){
							Product product = alProductByPage.get(num);
		
			%>
										
					<div class="col-md-3 md-col" style="margin-bottom:30px">
					<div class="col-md">
						<a href="<%=request.getContextPath() %>/SingleByProductServlet?maSanPham=<%=product.getMaSanPham()%>" class="compare-in"><img
							src="<%=request.getContextPath()%>/templates/public/images/<%=product.getHinhAnh() %>"
							alt="" />
							<div class="compare">
								<span>Add to Compare</span> <span>Add to Whislist</span>
							</div> </a>
						<div class="top-content">
							<h5>
								<a href="<%=request.getContextPath() %>/SingleByProductServlet?maSanPham=<%=product.getMaSanPham()%>"><%=product.getTenSanPham()%></a>
							</h5>
							<div class="white">
								<a href="<%=request.getContextPath() %>/CartServlet?command=plus&maSanPham=<%=product.getMaSanPham()%>"
									class="hvr-shutter-in-vertical hvr-shutter-in-vertical2">ADD
									TO CART</a>
								<p class="dollar">
									<span class="in-dollar">$</span><span><%=product.getGiaSanPham() %></span>
								</p>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
					<%
						}
					}
				%>
			</div>
			<div class="clearfix"></div>
			<%
				if(total>0){
			%>
			<ul class="start">
				<li><a href="<%=request.getContextPath()%>/ProductByCategoryServlet?maDanhMuc=<%=maDanhMuc%>&pages=<%=pages>1?pages-1:pages%>"><i></i></a></li>
				<%
					for(int i=1;i<=Math.ceil((double)total/8);i++){
				%>
				<li class="arrow"><a href="<%=request.getContextPath()%>/ProductByCategoryServlet?maDanhMuc=<%=maDanhMuc%>&pages=<%=i%>"><%=i %></a></li>
				<%
					}
				%>
				<li><a href="<%=request.getContextPath()%>/ProductByCategoryServlet?maDanhMuc=<%=maDanhMuc%>&pages=<%=pages<Math.ceil((double)total/2)?pages+1:pages%>"><i class="next"> </i></a></li>
			</ul>
			<%
				}else{
			%>
					<p style="color: red; margin: 0px 0px 30px 50px" class="start">Không có sản phẩm để hiển thị</p>
			<%
				}
			%>
		</div>
	</div>
	<jsp:include page="/templates/public/inc/footer.jsp"></jsp:include>
</body>
</html>