<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Wishlist</title>
</head>
<body>
	<jsp:include page="/templates/public/inc/header.jsp"></jsp:include>
	<div class="container">
		<div class="wish-list">
			<h6><a href="#">Product Name</a>	<a href="#">Unit Price</a>	<a href="#">Stock Status</a></h6>	
			<p>No products were added to the wishlist</p>
		</div>
	</div>
	<jsp:include page="/templates/public/inc/footer.jsp"></jsp:include>
</body>
</html>