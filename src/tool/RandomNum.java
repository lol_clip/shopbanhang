package tool;
import java.util.Random;
public class RandomNum {
	public int random(int min,int max){
		try {
			Random rd = new Random();
			int range = max-min+1;
			int randumNum = min+rd.nextInt(range);
			return randumNum;
		} catch (Exception e) {
			return -1;
		}
	}
}
