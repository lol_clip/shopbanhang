package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.bean.Product;

public class ProductDAO {
	private ConnectMySQL conn = new ConnectMySQL();
	private Statement stm;
	private PreparedStatement pstm;
	private ResultSet rs;
	private String sql;

	// Lấy danh sách sản phẩm theo mã danh mục
	public ArrayList<Product> getListProduct(int maDanhMuc) {
		ArrayList<Product> alProduct = new ArrayList<Product>();
		sql = "select * from Product where maTheLoai = ?";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setInt(1, maDanhMuc);
			rs = pstm.executeQuery();
			while (rs.next()) {
				Product product = new Product(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDouble(4),
						rs.getString(5), rs.getInt(6));
				alProduct.add(product);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return alProduct;
	}

	// Lấy sản phẩm theo mã sản phẩm
	public Product getProduct(int maSanPham) {
		Product product = null;
		sql = "select * from product where maSanPham = ?";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setInt(1, maSanPham);
			rs = pstm.executeQuery();
			if (rs.next()) {
				product = new Product(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDouble(4), rs.getString(5),
						rs.getInt(6));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return product;
	}

	// Lấy danh sách sản phẩm theo thể loại có phân trang
	public ArrayList<Product> getListProductByNav(int maTheLoai, int start, int end) {
		ArrayList<Product> alProduct = new ArrayList<Product>();
		sql = "SELECT * FROM product WHERE maTheLoai = '" + maTheLoai + "' limit ?,?";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setInt(1, start);
			pstm.setInt(2, end);
			rs = pstm.executeQuery();

			while (rs.next()) {
				Product product = new Product(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDouble(4),
						rs.getString(5), rs.getInt(6));
				alProduct.add(product);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return alProduct;
	}

	// Đếm số lượng sản phẩm mỗi loại
	public int countProductByCategory(int maTheLoai) {
		int count = 0;
		sql = "select count(maSanPham) from product where maTheLoai = ?";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setInt(1, maTheLoai);
			rs = pstm.executeQuery();
			if (rs.next()) {
				count = rs.getInt(1);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return count;
	}

	// Lấy danh sách sản phẩm
	public ArrayList<Product> getListProduct() {
		ArrayList<Product> alProduct = new ArrayList<Product>();
		Product product = null;
		sql = "select * from product";
		try {
			stm = conn.getConnection().createStatement();
			rs = stm.executeQuery(sql);
			while (rs.next()) {
				product = new Product(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDouble(4), rs.getString(5),
						rs.getInt(6));
				alProduct.add(product);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return alProduct;
	}

	// Kiểm tra thêm sản phẩm
	public boolean insertProduct(Product product) {
		boolean result = false;
		sql = "insert into product(tenSanPham,hinhAnh,giaSanPham,moTa,maTheLoai) values(?,?,?,?,?)";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setString(1, product.getTenSanPham());
			pstm.setString(2, product.getHinhAnh());
			pstm.setDouble(3, product.getGiaSanPham());
			pstm.setString(4, product.getMoTa());
			pstm.setInt(5, product.getmaDanhMuc());
			pstm.executeUpdate();
			result = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return result;
	}
	//Kiểm tra update sản phẩm
	public boolean updateProduct(Product product) {
		boolean result = false;
		sql = "update product set tenSanPham = ?,hinhAnh=?,giaSanPham=?,moTa=?,maTheLoai=? where maSanPham=?";
		try {
			pstm =conn.getConnection().prepareStatement(sql);
			pstm.setString(1, product.getTenSanPham());
			pstm.setString(2, product.getHinhAnh());
			pstm.setDouble(3, product.getGiaSanPham());
			pstm.setString(4, product.getMoTa());
			pstm.setInt(5, product.getmaDanhMuc());
			pstm.setInt(6, product.getMaSanPham());
			pstm.executeUpdate();
			result=true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return result;
	}
	//Kiểm tra xóa sản phẩm
	public boolean deleteProduct(int maSanPham) {
		boolean result = false;
		sql = "delete from product where maSanPham=?";
		try {
			pstm =conn.getConnection().prepareStatement(sql);
			pstm.setInt(1, maSanPham);
			pstm.executeUpdate();
			result=true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return result;
	}
}
