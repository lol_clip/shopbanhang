package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.bean.Bill;

public class BillDAO {
	private ConnectMySQL conn = new ConnectMySQL();
	private Statement stm;
	private PreparedStatement pstm;
	private ResultSet rs;
	private String sql;

	public boolean inSertBill(Bill bill) {
		boolean result = false;
		sql = "insert into bill(maHoaDon,tenThanhVien,tongTien,hinhThucThanhToan,diaChi,thoiGian) values(?,?,?,?,?,?)";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setInt(1, bill.getMaHoaDon());
			pstm.setString(2, bill.getTenThanhVien());
			pstm.setDouble(3, bill.getTongTien());
			pstm.setString(4, bill.getHinhThucThanhToan());
			pstm.setString(5, bill.getDiaChi());
			pstm.setTimestamp(6, bill.getThoiGian());
			pstm.executeUpdate();
			result = true;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}

	// Lấy danh sách hóa đơn
	public ArrayList<Bill> getListBill() {
		ArrayList<Bill> alBill = new ArrayList<Bill>();
		sql = "select * from bill";
		try {
			stm = conn.getConnection().createStatement();
			rs = stm.executeQuery(sql);
			while (rs.next()) {
				Bill bill = new Bill(rs.getInt(1), rs.getString(2), rs.getDouble(3), rs.getString(4), rs.getString(5),
						rs.getTimestamp(6));
				alBill.add(bill);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				stm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return alBill;
	}

	// Kiểm tra xóa hóa đơn
	public boolean deleteBill(int maHoaDon) {
		boolean result = false;
		sql = "delete from bill where maHoaDon = ?";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setInt(1, maHoaDon);
			pstm.executeUpdate();
			result = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return result;
	}
}
