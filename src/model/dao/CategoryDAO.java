package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.bean.Category;

public class CategoryDAO {
	private ConnectMySQL conn = new ConnectMySQL();
	private Statement stm;
	private PreparedStatement pstm;
	private ResultSet rs;
	private String sql;

	// Lấy danh sách thể loại
	public ArrayList<Category> getListCategory() {
		ArrayList<Category> alCategory = new ArrayList<Category>();
		Category category;
		sql = "select * from category where 1";
		try {
			stm = conn.getConnection().createStatement();
			rs = stm.executeQuery(sql);
			while (rs.next()) {
				category = new Category(rs.getInt(1), rs.getString(2), rs.getInt(3));
				alCategory.add(category);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				stm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return alCategory;
	}

	// Thêm thể loại mới
	public boolean insertCategory(Category category) {
		boolean result = false;
		sql = "insert into category(tenDanhMuc,maDSDanhMuc) value(?,?)";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setString(1, category.getTenDanhMuc());
			pstm.setInt(2, category.getMaDSDanhMuc());
			pstm.executeUpdate();
			result = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}

	// Cập nhật thể loại
	public boolean updateCategory(Category category) {
		boolean result = false;
		sql = "update category set tenDanhMuc = ?, maDSDanhMuc = ? where maDanhMuc = ?";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setString(1, category.getTenDanhMuc());
			pstm.setInt(2, category.getMaDSDanhMuc());
			pstm.setInt(3, category.getMaDanhMuc());
			pstm.executeUpdate();
			result = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return result;
	}

	// Xóa thể loại
	public boolean deleteCategory(int maDanhMuc) {
		boolean result = false;
		sql = "delete from category where maDanhMuc = ?";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setInt(1, maDanhMuc);
			pstm.executeUpdate();
			result = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return result;
	}
	//Lấy danh mục theo mã danh mục
	public Category getCategory(int maDanhMuc) {
		Category category =null;
		sql = "select * from category where maDanhMuc = ?";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setInt(1, maDanhMuc);
			rs = pstm.executeQuery();
			if(rs.next()){
				category = new Category(rs.getInt(1), rs.getString(2), rs.getInt(3));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return category;
	}
	//Lấy danh sách thể loại theo mã danh mục
	public ArrayList<Category> getListCategory(int maDSDanhMuc) {
		ArrayList<Category> alCategory = new ArrayList<Category>();
		Category category;
		sql = "select * from category where maDSDanhMuc=?";
		try {
			pstm =conn.getConnection().prepareStatement(sql);
			pstm.setInt(1, maDSDanhMuc);
			rs = pstm.executeQuery();
			while (rs.next()) {
				category = new Category(rs.getInt(1), rs.getString(2), rs.getInt(3));
				alCategory.add(category);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				rs.close();
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return alCategory;
	}
}
