package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.bean.User;

public class UserDAO {
	private ConnectMySQL conn = new ConnectMySQL();
	private Statement stm;
	private PreparedStatement pstm;
	private ResultSet rs;
	private String sql;
	
	//Phương thức kiểm tra tài khoản đã tồn tại hay chưa
	public boolean checkUserName(String userName){
		boolean result =false;
		sql="select userName from user where userName = ?";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setString(1, userName);
			rs = pstm.executeQuery();
			if(rs.next()){
				result = true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return result;
	}
	//Phương thức thêm mới user
	public boolean insertUser(User user){
		boolean result = false;
		sql = "insert into user(userName,password,userRole) values (?,?,?)";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setString(1, user.getUserName());
			pstm.setString(2, user.getPassword());
			pstm.setInt(3, user.getUserRole());
			pstm.executeUpdate();
			result =true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return result;
	}
	//Phương thức kiểm tra đăng nhập
	public boolean checkUser(User user) {
		boolean result =false;
		sql="select * from user where username = ? and password = ?";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setString(1, user.getUserName());
			pstm.setString(2, user.getPassword());
			rs = pstm.executeQuery();
			if(rs.next()){
				result = true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}
	//Phương thức kiểm tra Admin
	public boolean checkAdmin(String username, String password) {
		boolean result = false;
		sql="select * from user where username = ? and password=? and userRole=1";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setString(1, username);
			pstm.setString(2, password);
			rs = pstm.executeQuery();
			if(rs.next()){
				result=true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}
	//test
	public static void main(String[] args) {
		UserDAO userDAO = new UserDAO();
		//System.out.println(userDAO.checkAdmin("admin", "e10adc3949ba59abbe56e057f20f883e"));
	}
}
