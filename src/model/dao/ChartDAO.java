package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import model.bean.Category;
import model.bean.ListCategory;
import model.bean.Product;
import model.bean.Value;
import model.bo.CategoryBO;
import model.bo.ListCategoryBO;
import model.bo.ProductBO;

public class ChartDAO {
	ConnectMySQL conn = new ConnectMySQL();
	PreparedStatement pstm;
	Statement stm;
	ResultSet rs;
	String sql="";
	public ArrayList<Value> getAll(){
		ArrayList<Value> alValue = new ArrayList<Value>();
		try {
			ListCategoryBO listCategoryBO = new ListCategoryBO();
			CategoryBO categoryBO = new CategoryBO();
			ProductBO productBO = new ProductBO();
			ArrayList<ListCategory> alListCategory = listCategoryBO.getListMenuBar();
			ArrayList<Integer> alSize = new ArrayList<Integer>();
			for(ListCategory menuBar:alListCategory){
				ArrayList<Category> alCategory = categoryBO.getListCategory(menuBar.getMaDSDanhMuc());
				int size=0;
				for(Category category:alCategory){
					ArrayList<Product> alProduct = productBO.getListProduct(category.getMaDanhMuc());
					size += alProduct.size();
				}
				alSize.add(size);
			}
			for(int i=0;i<alListCategory.size();i++){
				Value value = new Value(alListCategory.get(i).getTenDSDanhMuc(), alSize.get(i));
				alValue.add(value);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
		return alValue;
	}
//	public static void main(String[] args) {
//		ChartDAO chartDAO = new ChartDAO();
//		ArrayList<Value> alValue = chartDAO.getAll();
//		for(int i=0;i<alValue.size();i++){
//			System.out.println(alValue.get(i).getName()+": "+alValue.get(i).getValue());
//		}
//		
//	}
}
