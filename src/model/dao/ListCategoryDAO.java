package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.sun.mail.handlers.text_html;

import model.bean.ListCategory;

public class ListCategoryDAO {
	private ConnectMySQL conn = new ConnectMySQL();
	private Statement stm;
	private PreparedStatement pstm;
	private ResultSet rs;
	private String sql;
	//Lấy danh sách menubar
	public ArrayList<ListCategory> getListMenuBar() {
		ArrayList<ListCategory> alListCategory = new ArrayList<ListCategory>();
		ListCategory listCategory;
		sql = "select * from listcategory where 1";
		try {
			stm = conn.getConnection().createStatement();
			rs = stm.executeQuery(sql);
			while(rs.next()){
				listCategory = new ListCategory(rs.getInt(1), rs.getString(2));
				alListCategory.add(listCategory);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try {
				rs.close();
				stm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return alListCategory;
	}
	//Thêm menubar
	public boolean insertMenuBar(String tenMenuBar) {
		boolean result = false;
		sql = "insert into listcategory(tenDSDanhMuc) value(?)";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setString(1, tenMenuBar);
			pstm.executeUpdate();
			result=true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return result;
	}
	//Lấy menuBar theo mã
	public ListCategory getMenuBar(int maMenuBar) {
		ListCategory menuBar = null;
		sql = "select * from listcategory where maDSDanhMuc = ?";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setInt(1, maMenuBar);
			rs = pstm.executeQuery();
			if(rs.next()){
				menuBar = new ListCategory(rs.getInt(1),rs.getString(2));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return menuBar;
	}
	//Cập nhật menubar
	public boolean updateMenuBar(ListCategory menuBar) {
		boolean result = false;
		sql = "update listcategory set tenDSDanhMuc = ? where maDSDanhMuc = ?";
		try {
			pstm =conn.getConnection().prepareStatement(sql);
			pstm.setString(1, menuBar.getTenDSDanhMuc());
			pstm.setInt(2, menuBar.getMaDSDanhMuc());
			pstm.executeUpdate();
			result=true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return result;
	}
	//Xóa menubar
	public boolean deleteMenuBar(int maMenuBar) {
		boolean result = false;
		sql = "delete from listcategory where maDSDanhMuc = ?";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setInt(1, maMenuBar);
			pstm.executeUpdate();
			result=true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return result;
	}

}
