package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.bean.BillDetail;

public class BillDetailDAO {
	private ConnectMySQL conn = new ConnectMySQL();
	private PreparedStatement pstm;
	private Statement stm;
	private ResultSet rs;
	private String sql;
	//Kiểm tra thêm chi tiết hóa đơn
	public boolean insertBillDetai(BillDetail billDetail){
		boolean result=false;
		sql = "insert into billdetail(maHoaDon,maSanPham,gia,soLuong) values(?,?,?,?)";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setInt(1, billDetail.getMaHoaDon());
			pstm.setInt(2, billDetail.getMaSanPham());
			pstm.setDouble(3, billDetail.getGia());
			pstm.setInt(4, billDetail.getSoLuong());
			pstm.executeUpdate();
			result=true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return result;
	}
	//Lấy danh sách chi tiết hóa đơn
	public ArrayList<BillDetail> getListBillDetail(int maHoaDon) {
		ArrayList<BillDetail> alBillDetail = new ArrayList<BillDetail>();
		sql = "select * from billdetail where maHoaDon = ?";
		try {
			pstm = conn.getConnection().prepareStatement(sql);
			pstm.setInt(1, maHoaDon);
			rs = pstm.executeQuery();
			while(rs.next()){
				BillDetail billDetail = new BillDetail(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getDouble(4), rs.getInt(5));
				alBillDetail.add(billDetail);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			try {
				pstm.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return alBillDetail;
	}
}
