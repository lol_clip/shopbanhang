package model.bean;

public class Product {
	private int maSanPham;
	private String tenSanPham;
	private String hinhAnh;
	private double giaSanPham;
	private String moTa;
	private int maDanhMuc;
	
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Product(int maSanPham, String tenSanPham, String hinhAnh, double giaSanPham, String moTa, int maDanhMuc) {
		super();
		this.maSanPham = maSanPham;
		this.tenSanPham = tenSanPham;
		this.hinhAnh = hinhAnh;
		this.giaSanPham = giaSanPham;
		this.moTa = moTa;
		this.maDanhMuc = maDanhMuc;
	}
	public Product(String tenSanPham, String hinhAnh, double giaSanPham, String moTa, int maDanhMuc) {
		super();
		this.tenSanPham = tenSanPham;
		this.hinhAnh = hinhAnh;
		this.giaSanPham = giaSanPham;
		this.moTa = moTa;
		this.maDanhMuc = maDanhMuc;
	}
	public int getMaSanPham() {
		return maSanPham;
	}
	public void setMaSanPham(int maSanPham) {
		this.maSanPham = maSanPham;
	}
	public String getTenSanPham() {
		return tenSanPham;
	}
	public void setTenSanPham(String tenSanPham) {
		this.tenSanPham = tenSanPham;
	}
	public String getHinhAnh() {
		return hinhAnh;
	}
	public void setHinhAnh(String hinhAnh) {
		this.hinhAnh = hinhAnh;
	}
	public double getGiaSanPham() {
		return giaSanPham;
	}
	public void setGiaSanPham(double giaSanPham) {
		this.giaSanPham = giaSanPham;
	}
	public String getMoTa() {
		return moTa;
	}
	public void setMoTa(String moTa) {
		this.moTa = moTa;
	}
	public int getmaDanhMuc() {
		return maDanhMuc;
	}
	public void setmaDanhMuc(int maDanhMuc) {
		this.maDanhMuc = maDanhMuc;
	}
}
