package model.bean;

public class ListCategory {
	private int maDSDanhMuc;
	private String tenDSDanhMuc;
	public ListCategory() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ListCategory(int maDSDanhMuc, String tenDSDanhMuc) {
		super();
		this.maDSDanhMuc = maDSDanhMuc;
		this.tenDSDanhMuc = tenDSDanhMuc;
	}
	public int getMaDSDanhMuc() {
		return maDSDanhMuc;
	}
	public void setMaDSDanhMuc(int maDSDanhMuc) {
		this.maDSDanhMuc = maDSDanhMuc;
	}
	public String getTenDSDanhMuc() {
		return tenDSDanhMuc;
	}
	public void setTenDSDanhMuc(String tenDSDanhMuc) {
		this.tenDSDanhMuc = tenDSDanhMuc;
	}
}
