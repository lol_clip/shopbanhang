package model.bean;

public class User {
	private int maThanhVien;
	private String userName;
	private String password;
	private int userRole;
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User(int maThanhVien, String userName, String password, int userRole) {
		super();
		this.maThanhVien = maThanhVien;
		this.userName = userName;
		this.password = password;
		this.userRole = userRole;
	}
	public User(String userName, String password, int userRole) {
		super();
		this.userName = userName;
		this.password = password;
		this.userRole = userRole;
	}
	public int getMaThanhVien() {
		return maThanhVien;
	}
	public void setMaThanhVien(int maThanhVien) {
		this.maThanhVien = maThanhVien;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getUserRole() {
		return userRole;
	}
	public void setUserRole(int userRole) {
		this.userRole = userRole;
	}
}
