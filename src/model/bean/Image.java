package model.bean;

public class Image {
	private int maHinhAnh;
	private String tenHinhAnh;
	private String link;
	public Image() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Image(int maHinhAnh, String tenHinhAnh, String link) {
		super();
		this.maHinhAnh = maHinhAnh;
		this.tenHinhAnh = tenHinhAnh;
		this.link = link;
	}
	public int getMaHinhAnh() {
		return maHinhAnh;
	}
	public void setMaHinhAnh(int maHinhAnh) {
		this.maHinhAnh = maHinhAnh;
	}
	public String getTenHinhAnh() {
		return tenHinhAnh;
	}
	public void setTenHinhAnh(String tenHinhAnh) {
		this.tenHinhAnh = tenHinhAnh;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
}
