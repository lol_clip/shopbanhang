package model.bean;

public class Item {
	private Product sanPham;
	private int soLuong;
	public Item() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Item(Product sanPham, int soLuong) {
		super();
		this.sanPham = sanPham;
		this.soLuong = soLuong;
	}
	public Product getSanPham() {
		return sanPham;
	}
	public void setSanPham(Product sanPham) {
		this.sanPham = sanPham;
	}
	public int getSoLuong() {
		return soLuong;
	}
	public void setSoLuong(int soLuong) {
		this.soLuong = soLuong;
	}
}
