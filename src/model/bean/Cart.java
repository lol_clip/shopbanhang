package model.bean;

import java.util.HashMap;
import java.util.Map;

public class Cart {
	private HashMap<Integer, Item> cartItem;

	public Cart(HashMap<Integer, Item> cartItem) {
		super();
		this.cartItem = cartItem;
	}

	public Cart() {
		super();
		this.cartItem = new HashMap<>();
	}

	public HashMap<Integer, Item> getCartItem() {
		return cartItem;
	}

	public void setCartItem(HashMap<Integer, Item> cartItem) {
		this.cartItem = cartItem;
	}
	
	//Thêm vào giỏ hàng
	public void putToCart(int key, Item item){
		boolean check = cartItem.containsKey(key);
		if(check){
			int soLuongCu = item.getSoLuong();
			item.setSoLuong(soLuongCu+1);
			cartItem.put(key, item);
		}else {
			cartItem.put(key, item);
		}
	}
	//Lấy ra khỏi giỏ hàng
	public void popToCart(int key, Item item){
		boolean check = cartItem.containsKey(key);
		if(check){
			int soLuongCu = item.getSoLuong();
			if(soLuongCu<=1){
				cartItem.remove(key);
			}else{
				item.setSoLuong(soLuongCu-1);
				cartItem.put(key, item);
			}
		}
	}
	//Xóa khỏi giỏ hàng
	public void removeToCard(int key){
		boolean check = cartItem.containsKey(key);
		if(check){
			cartItem.remove(key);
		}
	}
	//Đếm số lượng
	public int countItem(){
		return cartItem.size();
	}
	//Tổng số tiền
	public double totalCart(){
		double count=0;
		for(Map.Entry<Integer, Item> list:cartItem.entrySet()){
			count += list.getValue().getSanPham().getGiaSanPham()*list.getValue().getSoLuong();
		}
		return count;
	}
}
