package model.bean;

public class Category {
	private int maDanhMuc;
	private String tenDanhMuc;
	private int maDSDanhMuc;
	public Category() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Category(int maDanhMuc, String tenDanhMuc, int maDSDanhMuc) {
		super();
		this.maDanhMuc = maDanhMuc;
		this.tenDanhMuc = tenDanhMuc;
		this.maDSDanhMuc = maDSDanhMuc;
	}
	public Category(String tenDanhMuc, int maDSDanhMuc) {
		super();
		this.tenDanhMuc = tenDanhMuc;
		this.maDSDanhMuc = maDSDanhMuc;
	}
	public int getMaDanhMuc() {
		return maDanhMuc;
	}
	public void setMaDanhMuc(int maDanhMuc) {
		this.maDanhMuc = maDanhMuc;
	}
	public String getTenDanhMuc() {
		return tenDanhMuc;
	}
	public void setTenDanhMuc(String tenDanhMuc) {
		this.tenDanhMuc = tenDanhMuc;
	}
	public int getMaDSDanhMuc() {
		return maDSDanhMuc;
	}
	public void setMaDSDanhMuc(int maDSDanhMuc) {
		this.maDSDanhMuc = maDSDanhMuc;
	}
}
