package model.bean;

public class BillDetail {
	private int maChiTietHoaDon;
	private int maHoaDon;
	private int maSanPham;
	private double gia;
	private int soLuong;
	public BillDetail() {
		super();
		// TODO Auto-generated constructor stub
	}
	public BillDetail(int maChiTietHoaDon, int maHoaDon, int maSanPham, double gia, int soLuong) {
		super();
		this.maChiTietHoaDon = maChiTietHoaDon;
		this.maHoaDon = maHoaDon;
		this.maSanPham = maSanPham;
		this.gia = gia;
		this.soLuong = soLuong;
	}
	public BillDetail(int maHoaDon, int maSanPham, double gia, int soLuong) {
		super();
		this.maHoaDon = maHoaDon;
		this.maSanPham = maSanPham;
		this.gia = gia;
		this.soLuong = soLuong;
	}
	public int getMaChiTietHoaDon() {
		return maChiTietHoaDon;
	}
	public void setMaChiTietHoaDon(int maChiTietHoaDon) {
		this.maChiTietHoaDon = maChiTietHoaDon;
	}
	public int getMaHoaDon() {
		return maHoaDon;
	}
	public void setMaHoaDon(int maHoaDon) {
		this.maHoaDon = maHoaDon;
	}
	public int getMaSanPham() {
		return maSanPham;
	}
	public void setMaSanPham(int maSanPham) {
		this.maSanPham = maSanPham;
	}
	public double getGia() {
		return gia;
	}
	public void setGia(double gia) {
		this.gia = gia;
	}
	public int getSoLuong() {
		return soLuong;
	}
	public void setSoLuong(int soLuong) {
		this.soLuong = soLuong;
	}
}
