package model.bean;

import java.sql.Timestamp;

public class Bill {
	private int maHoaDon;
	private String tenThanhVien;
	private double tongTien;
	private String hinhThucThanhToan;
	private String diaChi;
	private Timestamp thoiGian;
	public Bill() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Bill(int maHoaDon, String tenThanhVien, double tongTien, String hinhThucThanhToan, String diaChi,
			Timestamp thoiGian) {
		super();
		this.maHoaDon = maHoaDon;
		this.tenThanhVien = tenThanhVien;
		this.tongTien = tongTien;
		this.hinhThucThanhToan = hinhThucThanhToan;
		this.diaChi = diaChi;
		this.thoiGian = thoiGian;
	}
	public Bill(String tenThanhVien, double tongTien, String hinhThucThanhToan, String diaChi,
			Timestamp thoiGian) {
		super();
		this.tenThanhVien = tenThanhVien;
		this.tongTien = tongTien;
		this.hinhThucThanhToan = hinhThucThanhToan;
		this.diaChi = diaChi;
		this.thoiGian = thoiGian;
	}
	public int getMaHoaDon() {
		return maHoaDon;
	}
	public void setMaHoaDon(int maHoaDon) {
		this.maHoaDon = maHoaDon;
	}
	public String getTenThanhVien() {
		return tenThanhVien;
	}
	public void setMaThanhVien(String tenThanhVien) {
		this.tenThanhVien = tenThanhVien;
	}
	public double getTongTien() {
		return tongTien;
	}
	public void setTongTien(double tongTien) {
		this.tongTien = tongTien;
	}
	public String getHinhThucThanhToan() {
		return hinhThucThanhToan;
	}
	public void setHinhThucThanhToan(String hinhThucThanhToan) {
		this.hinhThucThanhToan = hinhThucThanhToan;
	}
	public String getDiaChi() {
		return diaChi;
	}
	public void setDiaChi(String diaChi) {
		this.diaChi = diaChi;
	}
	public Timestamp getThoiGian() {
		return thoiGian;
	}
	public void setThoiGian(Timestamp thoiGian) {
		this.thoiGian = thoiGian;
	}
}
