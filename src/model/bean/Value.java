package model.bean;

public class Value {
	private String name;
	private int value;
	public Value() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Value(String name, int value) {
		super();
		this.name = name;
		this.value = value;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String toString() {
		return name;
	}
}
