package model.bo;

import java.util.ArrayList;

import model.bean.Bill;
import model.dao.BillDAO;

public class BillBO {
	private BillDAO billDAO = new BillDAO();
	//Kiểm tra thêm hóa đơn
	public boolean insertBill(Bill bill) {
		return billDAO.inSertBill(bill);
	}
	//Lấy danh sách hóa đơn
	public ArrayList<Bill> getListBill() {
		return billDAO.getListBill();
	}
	//Kiểm tra xóa hóa đơn
	public boolean deleteBill(int maHoaDon) {
		return billDAO.deleteBill(maHoaDon);
	}
	
	
}
