package model.bo;

import java.util.ArrayList;

import model.bean.Category;
import model.dao.CategoryDAO;

public class CategoryBO {
	private CategoryDAO categoryDAO = new CategoryDAO();
	//Lấy danh sách thể loại
	public ArrayList<Category> getListCategory() {
		return categoryDAO.getListCategory();
	}
	//Thêm thể loại mới
	public boolean insertCategory(Category category){
		return categoryDAO.insertCategory(category);
	}
	//Cập nhật thể loại
	public boolean updateCategory(Category category){
		return categoryDAO.updateCategory(category);
	}
	//Xóa thể loại
	public boolean  deleteCategory(int maDanhMuc){
		return categoryDAO.deleteCategory(maDanhMuc);
	}
	//Lấy danh mục theo mã danh mục
	public Category getCategory(int maDanhMuc) {
		return categoryDAO.getCategory(maDanhMuc);
	}
	//Lấy danh sách thể loại theo danh mục
	public ArrayList<Category> getListCategory(int maDSDanhMuc) {
		return categoryDAO.getListCategory(maDSDanhMuc);
	}
}
