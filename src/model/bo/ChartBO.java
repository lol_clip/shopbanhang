package model.bo;

import java.util.ArrayList;

import model.bean.Value;
import model.dao.ChartDAO;

public class ChartBO {
	private ChartDAO chartDAO = new ChartDAO();
	public ArrayList<Value> getAll(){
		return chartDAO.getAll();
	}
}
