package model.bo;

import java.util.ArrayList;

import model.bean.ListCategory;
import model.dao.ListCategoryDAO;

public class ListCategoryBO {
	private ListCategoryDAO listCategoryDAO = new ListCategoryDAO();
	//Lấy danh sách menubar
	public ArrayList<ListCategory> getListMenuBar() {
		return listCategoryDAO.getListMenuBar();	
	}
	//Thêm menubar
	public boolean insertMenuBar(String tenMenuBar) {
		return listCategoryDAO.insertMenuBar(tenMenuBar);
	}
	//Lấy menubar theo mã
	public ListCategory getMenuBar(int maMenuBar) {
		return listCategoryDAO.getMenuBar(maMenuBar);
	}
	//Cập nhật menubar
	public boolean updateMenuBar(ListCategory menuBar) {
		return listCategoryDAO.updateMenuBar(menuBar);
	}
	//Xóa menubar
	public boolean deleteMenuBar(int maMenuBar) {
		return listCategoryDAO.deleteMenuBar(maMenuBar);
	}


}
