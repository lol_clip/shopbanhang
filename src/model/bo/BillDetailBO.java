package model.bo;

import java.util.ArrayList;

import model.bean.BillDetail;
import model.dao.BillDetailDAO;

public class BillDetailBO {
	private BillDetailDAO billDetailDAO = new BillDetailDAO();
	//Kiểm tra thêm chi tiết hóa đơn
	public boolean insertBillDetai(BillDetail billDetail){
		return billDetailDAO.insertBillDetai(billDetail);
	}
	//Lấy chi tiết hóa đơn
	public ArrayList<BillDetail> getListBillDetail(int maHoaDon) {
		return billDetailDAO.getListBillDetail(maHoaDon);
	}
}
