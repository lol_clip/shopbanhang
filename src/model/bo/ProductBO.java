package model.bo;

import java.util.ArrayList;

import model.bean.Product;
import model.dao.ProductDAO;

public class ProductBO {
	private ProductDAO productDAO = new ProductDAO();
	//Lấy danh sách sản phẩm theo từng danh mục
	public ArrayList<Product> getListProduct(int maDanhMuc) {
		return productDAO.getListProduct(maDanhMuc);
	}
	//Lấy sản phẩm theo mã
	public Product getProduct(int maSanPham){
		return productDAO.getProduct(maSanPham);
	}
	//Lấy danh sách sản phẩm có phân trang
	public ArrayList<Product> getListProductByNav(int maTheLoai, int start, int end){
		return productDAO.getListProductByNav(maTheLoai,start,end);
	}
	//Đếm số sản phẩm trong mỗi danh mục
	public int countProductByCategory(int maTheLoai){
		return productDAO.countProductByCategory(maTheLoai);
	}
	//Lấy danh sách sản phẩm
	public ArrayList<Product> getListProduct() {
		return productDAO.getListProduct();
	}
	//Kiểm tra thêm sản phẩm
	public boolean insertProduct(Product product) {
		return productDAO.insertProduct(product);
	}
	//Kiểm tra update sản phẩm
	public boolean updateProduct(Product product) {
		return productDAO.updateProduct(product);
	}
	//Kiểm tra xóa sản phẩm
	public boolean deleteProduct(int maSanPham) {
		return productDAO.deleteProduct(maSanPham);
	}
}
