package model.bo;

import model.bean.User;
import model.dao.UserDAO;

public class UserBO {
	private UserDAO userDAO = new UserDAO();
	//Kiểm tra tên tồn tại hay chưa
	public boolean checkUserName(String userName){
		return userDAO.checkUserName(userName);
	}
	//Thêm thành viên
	public boolean insertUser(User user) {
		return userDAO.insertUser(user);
	}
	//Kiểm tra thành viên
	public boolean checkUser(User user) {
		return userDAO.checkUser(user);
	}
	//Kiểm tra có phải là Admin không
	public boolean checkAdmin(String username, String password) {
		return userDAO.checkAdmin(username,password);
	}
}
