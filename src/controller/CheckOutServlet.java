package controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.Bill;
import model.bean.BillDetail;
import model.bean.Cart;
import model.bean.Item;
import model.bo.BillBO;
import model.bo.BillDetailBO;
import tool.SendMail;

/**
 * Servlet implementation class CheckOutServlet
 */
public class CheckOutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckOutServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String command= new String(request.getParameter("command").getBytes("ISO-8859-1"), "UTF-8");
		if(command.equals("null")){
			request.getRequestDispatcher("/checkout.jsp").forward(request, response);
		}
		else{
			//command=CheckOut
			BillBO billBO = new BillBO();
			BillDetailBO billDetailBO = new BillDetailBO();
			String payment = request.getParameter("payment");
			String address = request.getParameter("address");
			HttpSession session = request.getSession();
			Cart cart = (Cart)session.getAttribute("cart");
			String user = (String)session.getAttribute("user");
			Timestamp thoiGian = new Timestamp(new Date().getTime());
			int maHoaDon = (int)new Date().getTime();
			try {
				if(cart==null||cart.countItem()==0){
					request.setAttribute("message", "Bạn chưa có sản phẩm nào trong giỏ hàng.");
					request.getRequestDispatcher("/message.jsp").forward(request, response);
				}
				else{
					Bill bill = new Bill(maHoaDon,user, cart.totalCart(), payment, address, thoiGian);
					billBO.insertBill(bill);
					for(Map.Entry<Integer, Item> list:cart.getCartItem().entrySet()){
						billDetailBO.insertBillDetai(new BillDetail(maHoaDon, list.getValue().getSanPham().getmaDanhMuc()
								, list.getValue().getSanPham().getGiaSanPham(), list.getValue().getSoLuong()));
					}
					SendMail sendMail = new SendMail();
					sendMail.sendMail(address, "Thanh toán đơn hàng từ Webbanhang", "Xác nhận thanh toán: \n"
							+ "Số lượng mặt hàng:"+cart.countItem()+"\n"
							+ "Tổng số tiền: $"+cart.totalCart());
					cart = new Cart();
					session.setAttribute("cart", cart);
					request.getRequestDispatcher("/index.jsp").forward(request, response);
				}
			} catch (Exception e) {
				request.setAttribute("message", "Đã có lỗi xãy ra.");
				request.getRequestDispatcher("/message.jsp").forward(request, response);
			}
		}
	}

}
