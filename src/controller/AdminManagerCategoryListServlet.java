package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.ListCategory;
import model.bo.ListCategoryBO;

/**
 * Servlet implementation class AdminManagerCategoryListServlet
 */
public class AdminManagerCategoryListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminManagerCategoryListServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ListCategoryBO listCategoryBO = new ListCategoryBO();
		ArrayList<ListCategory> alListCategory = new ArrayList<ListCategory>();
		String message = "";
		int maMenuBar = 0;
		boolean result =false;
		String tenMenuBar="";
		String command = request.getParameter("command");
		switch (command) {
		case "Them":
			request.getRequestDispatcher("/admin/insert-category-list.jsp").forward(request, response);
			break;
		case "Sua":
			maMenuBar = Integer.parseInt(request.getParameter("maMenuBar"));
			ListCategory menuBar = listCategoryBO.getMenuBar(maMenuBar);
			request.setAttribute("menuBar", menuBar);
			request.getRequestDispatcher("/admin/update-category-list.jsp").forward(request, response);
			break;
		case "Xoa":
			maMenuBar = Integer.parseInt(request.getParameter("maMenuBar"));
			result = listCategoryBO.deleteMenuBar(maMenuBar);
			if(result==true){
				message = "Xóa thành công.";
			}
			else{
				message = "Xóa không thành công.";
			}
			alListCategory = listCategoryBO.getListMenuBar();
			request.setAttribute("alListCategory", alListCategory);
			request.setAttribute("message", message);
			request.getRequestDispatcher("/admin/manager-category-list.jsp").forward(request, response);
			break;
		case "insertCategoryList":
			tenMenuBar = new String(request.getParameter("tenMenuBar").getBytes("ISO-8859-1"), "UTF-8") ;
			result = listCategoryBO.insertMenuBar(tenMenuBar);
			if(result==true){
				message = "Thêm thành công";
			}
			else {
				message ="Thêm không thành công";
			}
			alListCategory = listCategoryBO.getListMenuBar();
			request.setAttribute("message", message);
			request.setAttribute("alListCategory", alListCategory);
			request.getRequestDispatcher("/admin/manager-category-list.jsp").forward(request, response);
			break;
		case "updateCategoryList":
			maMenuBar = Integer.parseInt(request.getParameter("maMenuBar"));
			tenMenuBar = new String(request.getParameter("tenMenuBar").getBytes("ISO-8859-1"), "UTF-8") ;
			menuBar = new ListCategory(maMenuBar,tenMenuBar);
			result = listCategoryBO.updateMenuBar(menuBar);
			if(result==true){
				message = "Cập nhật thành công.";
			}
			else{
				message = "Cập nhật không thành công.";
			}
			alListCategory = listCategoryBO.getListMenuBar();
			request.setAttribute("alListCategory", alListCategory);
			request.setAttribute("message", message);
			request.getRequestDispatcher("/admin/manager-category-list.jsp").forward(request, response);
			break;

		default:
			alListCategory = listCategoryBO.getListMenuBar();
			request.setAttribute("alListCategory", alListCategory);
			request.getRequestDispatcher("/admin/manager-category-list.jsp").forward(request, response);
			break;
		}
		
	}

}
