package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.Product;
import model.bo.ProductBO;

/**
 * Servlet implementation class ProductByCategoryServlet
 */
public class ProductByCategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ProductByCategoryServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int maDanhMuc = Integer.parseInt(request.getParameter("maDanhMuc"));
		ProductBO productBO = new ProductBO();
		ArrayList<Product> alProduct = productBO.getListProduct(maDanhMuc);
		int pages = 0, start=0,end = 0,total =0;
		if(request.getParameter("pages")!=null){
			pages = Integer.parseInt(request.getParameter("pages"));
		}
		total = productBO.countProductByCategory(maDanhMuc);
		if(total<=8){
			start = 0;
			end =total;
		}
		else {
			start = (pages-1)*8;
			end = 8;
		}
		ArrayList<Product> alProductByPage = productBO.getListProductByNav(maDanhMuc, start, end);
		request.setAttribute("alProduct", alProduct);
		request.setAttribute("alProductByPage", alProductByPage);
		request.setAttribute("pages", pages);
		request.setAttribute("total", total);
		request.setAttribute("maDanhMuc", maDanhMuc);
		request.getRequestDispatcher("products.jsp").forward(request, response);
	}

}
