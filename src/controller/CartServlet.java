package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.Cart;
import model.bean.Item;
import model.bean.Product;
import model.bo.ProductBO;

/**
 * Servlet implementation class CartServlet
 */
public class CartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CartServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String command = request.getParameter("command");
		int maSanPham = Integer.parseInt(request.getParameter("maSanPham"));
		Cart cart = (Cart) session.getAttribute("cart");
		try {
			ProductBO productBO = new ProductBO();
			Product product = productBO.getProduct(maSanPham);
			switch (command) {
			case "plus":
				if(cart.getCartItem().containsKey(maSanPham)){
					cart.putToCart(maSanPham, new Item(product, cart.getCartItem().get(maSanPham).getSoLuong()));
				}
				else {
					cart.putToCart(maSanPham, new Item(product, 1));
				}
				break;
			case "remove":
				cart.removeToCard(maSanPham);
				break;
			default:
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect(request.getContextPath()+"/index.jsp");
		}
		session.setAttribute("cart", cart);
		response.sendRedirect(request.getContextPath()+"/index.jsp");
	}

}
