package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.bean.User;
import model.bo.UserBO;
import tool.MD5;

/**
 * Servlet implementation class UserControllerServlet
 */
public class UserControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserControllerServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserBO userBO = new UserBO();
		String command = "" + new String(request.getParameter("command").getBytes("ISO-8859-1"), "UTF-8");
		String url = "";
		String userName;
		String password;
		int userRole;
		User user;
		boolean result;
		switch (command) {
		case "Login":
			url ="login.jsp";
			break;
		case "Register":
			url = "register.jsp";
			break;
		case "InsertUser":
			userName = request.getParameter("username");
			password = request.getParameter("password");
			userRole = 0;
			user = new User(userName, password, userRole);
			if(userBO.checkUserName(userName)){
				result = userBO.insertUser(user);
				System.out.println(result);
				if (result==false) {
					url = "login.jsp";
				}else{
					url = "register.jsp";
				}
			}else{
				url ="register.jsp";
			}
			break;
		case "LoginUser":
			userName = request.getParameter("username");
			password = MD5.encryption(request.getParameter("password"));
			userRole = 0;
			user = new User(userName, password, userRole);
			result = userBO.checkUser(user);
			System.out.println(result);
			if (result) {
				HttpSession session = request.getSession();
				session.setAttribute("user", userName);
				url = "index.jsp";
			}else{
				request.setAttribute("error","Username hoặc password không đúng.");
				url = "login.jsp";
			}
			break;
		default:
			break;
		}
		request.getRequestDispatcher(url).forward(request, response);
	}

}
