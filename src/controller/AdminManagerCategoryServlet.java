package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.Category;
import model.bean.ListCategory;
import model.bo.CategoryBO;
import model.bo.ListCategoryBO;

/**
 * Servlet implementation class AdminManagerCategoryServlet
 */
public class AdminManagerCategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminManagerCategoryServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CategoryBO categoryBO = new CategoryBO();
		String command=request.getParameter("command");
		int maDanhMuc=0;
		String message="";
		boolean result =false;
		if(command.equals("Them")){
			ListCategoryBO listCategoryBO= new ListCategoryBO();
			ArrayList<ListCategory> alListCategory = listCategoryBO.getListMenuBar();
			request.setAttribute("alListCategory", alListCategory);
			request.getRequestDispatcher("/admin/insert-category.jsp").forward(request, response);
		}
		else if(command.equals("Sua")){
			maDanhMuc = Integer.parseInt(request.getParameter("maDanhMuc"));
			Category category = categoryBO.getCategory(maDanhMuc);
			ListCategoryBO listCategoryBO= new ListCategoryBO();
			ArrayList<ListCategory> alListCategory = listCategoryBO.getListMenuBar();
			request.setAttribute("alListCategory", alListCategory);
			request.setAttribute("category", category);
			request.getRequestDispatcher("/admin/update-category.jsp").forward(request, response);
		}
		else if(command.equals("Xoa")){
			maDanhMuc = Integer.parseInt(request.getParameter("maDanhMuc"));
			result = categoryBO.deleteCategory(maDanhMuc);
			if(result==true){
				message = "Xóa thành công.";
			}
			else{
				message = "Xóa không thành công.";
			}
			request.setAttribute("message", message);
			ArrayList<Category> alCategory = categoryBO.getListCategory();
			request.setAttribute("alCategory", alCategory);
			request.getRequestDispatcher("/admin/manager-category.jsp").forward(request, response);
		}
		else if(command.equals("insertCategory")){
			String tenDanhMuc=new String(request.getParameter("tenDanhMuc").getBytes("ISO-8859-1"), "UTF-8") ;
			int maDSDanhMuc = Integer.parseInt(request.getParameter("theLoai"));
			Category category = new Category(tenDanhMuc, maDSDanhMuc);
			result = categoryBO.insertCategory(category);
			if(result==true){
				message = "Thêm thành công.";
			}
			else{
				message = "Thêm không thành công.";
			}
			request.setAttribute("message", message);
			ArrayList<Category> alCategory = categoryBO.getListCategory();
			request.setAttribute("alCategory", alCategory);
			request.getRequestDispatcher("/admin/manager-category.jsp").forward(request, response);
		}
		else if(command.equals("updateCategory")){
			maDanhMuc = Integer.parseInt(request.getParameter("maDanhMuc"));
			String tenDanhMuc= new String(request.getParameter("tenDanhMuc").getBytes("ISO-8859-1"), "UTF-8") ;
			int maDSDanhMuc = Integer.parseInt(request.getParameter("theLoai"));
			Category category = new Category(maDanhMuc, tenDanhMuc, maDSDanhMuc);
			result = categoryBO.updateCategory(category);
			if(result==true){
				message = "Cập nhật thành công.";
			}
			else{
				message = "Cập nhật không thành công.";
			}
			request.setAttribute("message", message);
			ArrayList<Category> alCategory = categoryBO.getListCategory();
			request.setAttribute("alCategory", alCategory);
			request.getRequestDispatcher("/admin/manager-category.jsp").forward(request, response);
		}
		else{
			ArrayList<Category> alCategory = categoryBO.getListCategory();
			request.setAttribute("alCategory", alCategory);
			request.getRequestDispatcher("/admin/manager-category.jsp").forward(request, response);
		}
	}

}
