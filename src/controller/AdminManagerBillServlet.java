package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.Bill;
import model.bean.BillDetail;
import model.bo.BillBO;
import model.bo.BillDetailBO;

/**
 * Servlet implementation class AdminManagerBillServlet
 */
public class AdminManagerBillServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminManagerBillServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String command= new String(request.getParameter("command").getBytes("ISO-8859-1"),"UTF-8");
		BillBO billBO = new BillBO();
		ArrayList<Bill> alBill = new ArrayList<Bill>();
		BillDetailBO billDetailBO = new BillDetailBO();
		ArrayList<BillDetail> alBillDetail = new ArrayList<BillDetail>(); 
		
		switch (command) {
		case "Xem":
			int maHoaDon = Integer.parseInt(request.getParameter("maHoaDon"));
			alBillDetail = billDetailBO.getListBillDetail(maHoaDon);
			request.setAttribute("alBillDetail", alBillDetail);
			request.getRequestDispatcher("/admin/billDetail.jsp").forward(request, response);
			break;
		case "Xoa":
			maHoaDon = Integer.parseInt(request.getParameter("maHoaDon"));
			String message="";
			boolean result = billBO.deleteBill(maHoaDon);
			if(result==true){
				message="Xóa thành công";
			}
			else{
				message="Xóa không thành công";
			}
			alBill = billBO.getListBill();
			request.setAttribute("alBill", alBill);
			request.setAttribute("message", message);
			request.getRequestDispatcher("/admin/manager-bill.jsp").forward(request, response);
			break;	
		default:
			alBill = billBO.getListBill();
			request.setAttribute("alBill", alBill);
			request.getRequestDispatcher("/admin/manager-bill.jsp").forward(request, response);
			break;
		}
	}

}
