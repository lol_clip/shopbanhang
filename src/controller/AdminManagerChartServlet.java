package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.Category;
import model.bean.ListCategory;
import model.bean.Product;
import model.bean.Value;
import model.bo.CategoryBO;
import model.bo.ChartBO;
import model.bo.ListCategoryBO;
import model.bo.ProductBO;

/**
 * Servlet implementation class AdminManagerChartServlet
 */
public class AdminManagerChartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminManagerChartServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String command = new String(request.getParameter("command").getBytes("ISO-8859-1"),"UTF-8");
		if(command.equals("ThongKe")){
			ChartBO chartBO = new ChartBO();
			ArrayList<Value> alValues = chartBO.getAll();
			for(int i=0;i<alValues.size();i++){
				System.out.println(alValues.get(i).getName()+": "+alValues.get(i).getValue());
			}
			request.setAttribute("alValue", alValues);
			request.getRequestDispatcher("/admin/manager-chart.jsp").forward(request, response);
		}
	}

}
