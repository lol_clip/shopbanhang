package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.Category;
import model.bean.Product;
import model.bo.CategoryBO;
import model.bo.ProductBO;

/**
 * Servlet implementation class AdminManagerProductServlet
 */
public class AdminManagerProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminManagerProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProductBO productBO = new ProductBO();
		CategoryBO categoryBO = new CategoryBO();
		ArrayList<Product> alProduct = new ArrayList<Product>();
		ArrayList<Category> alCategory = new ArrayList<Category>();
		Product product = new Product();
		boolean result = false;
		String message ="";
		String command= request.getParameter("command");
		switch (command) {
		case "Them":
			alCategory = categoryBO.getListCategory();
			request.setAttribute("alCategory", alCategory);
			request.getRequestDispatcher("/admin/insert-product.jsp").forward(request, response);;
			break;
		case "Sua":
			int maSanPham = Integer.parseInt(request.getParameter("maSanPham"));
			product = productBO.getProduct(maSanPham);
			alCategory = categoryBO.getListCategory();
			request.setAttribute("alCategory", alCategory);
			request.setAttribute("product", product);
			request.getRequestDispatcher("/admin/update-product.jsp").forward(request, response);
			break;	
		case "Xoa":
			maSanPham = Integer.parseInt(request.getParameter("maSanPham"));
			result = productBO.deleteProduct(maSanPham);
			if(result==true){
				message = "Xóa thành công.";
			}
			else{
				message = "Xóa không thành công.";
			}
			alProduct = productBO.getListProduct();
			request.setAttribute("alProduct", alProduct);
			request.setAttribute("message", message);
			request.getRequestDispatcher("/admin/manager-product.jsp").forward(request, response);
			break;
		case "insertProduct":
			String tenSanPham = new String(request.getParameter("tenSanPham").getBytes("ISO-8859-1"),"UTF-8");
			String hinhAnh = new String(request.getParameter("hinhAnh").getBytes("ISO-8859-1"), "UTF-8");
			double giaSanPham = Double.parseDouble(request.getParameter("giaSanPham"));
			String moTa = new String(request.getParameter("moTa").getBytes("ISO-8859-1"), "UTF-8") ;
			int maTheLoai = Integer.parseInt(request.getParameter("theLoai"));
			product = new Product(tenSanPham, hinhAnh, giaSanPham, moTa, maTheLoai);
			result = productBO.insertProduct(product);
			if(result==true){
				message = "Thêm thành công.";
			}
			else{
				message = "Thêm không thành công.";
			}
			alProduct = productBO.getListProduct();
			request.setAttribute("alProduct", alProduct);
			request.setAttribute("message", message);
			request.getRequestDispatcher("/admin/manager-product.jsp").forward(request, response);
			break;
		case "updateProduct":
			maSanPham = Integer.parseInt(request.getParameter("maSanPham"));
			tenSanPham = new String(request.getParameter("tenSanPham").getBytes("ISO-8859-1"),"UTF-8");
			hinhAnh = new String(request.getParameter("hinhAnh").getBytes("ISO-8859-1"), "UTF-8");
			giaSanPham = Double.parseDouble(request.getParameter("giaSanPham"));
			moTa = new String(request.getParameter("moTa").getBytes("ISO-8859-1"), "UTF-8") ;
			maTheLoai = Integer.parseInt(request.getParameter("theLoai"));
			product = new Product(maSanPham,tenSanPham, hinhAnh, giaSanPham, moTa, maTheLoai);
			result = productBO.updateProduct(product);
			if(result==true){
				message = "Cập nhật thành công.";
			}
			else{
				message = "Cập nhật không thành công.";
			}
			alProduct = productBO.getListProduct();
			request.setAttribute("alProduct", alProduct);
			request.setAttribute("message", message);
			request.getRequestDispatcher("/admin/manager-product.jsp").forward(request, response);
			break;
		default:
			alProduct = productBO.getListProduct();
			request.setAttribute("alProduct", alProduct);
			request.getRequestDispatcher("/admin/manager-product.jsp").forward(request, response);
			break;
		}
	}

}
