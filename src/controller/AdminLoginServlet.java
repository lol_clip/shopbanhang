package controller;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

import model.bo.UserBO;
import tool.MD5;

/**
 * Servlet implementation class AdminLoginServlet
 */
public class AdminLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String command = request.getParameter("command");
		if(command.equals("Login")){
			String username = request.getParameter("username");
			String password = MD5.encryption(request.getParameter("password"));
			UserBO userBO = new UserBO();
			boolean checkAdmin = userBO.checkAdmin(username,password);
			if(checkAdmin==true){
				session.setAttribute("admin", username);
				request.getRequestDispatcher("/admin/index.jsp").forward(request, response);
			}else{
				request.getRequestDispatcher("/admin/login.jsp").forward(request, response);
			}
		}
		else{
			session.invalidate();
			request.getRequestDispatcher("/admin/login.jsp").forward(request, response);
		}
	}

}
