package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.bean.Product;
import model.bo.ProductBO;
import tool.RandomNum;

/**
 * Servlet implementation class IndexServlet
 */
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IndexServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProductBO productBO = new ProductBO();
		ArrayList<Product> alProduct = productBO.getListProduct();
		RandomNum randomNum = new RandomNum();
		if(alProduct!=null){
			if(alProduct.size()<5){
				if(alProduct.size()==1){
					Product product = alProduct.get(1);
					request.setAttribute("product", product);
				}
				else{
					int num = randomNum.random(1, alProduct.size());
					Product product = alProduct.get(num);
					request.setAttribute("product", product);
				}
				request.setAttribute("alFeature", alProduct);
				request.setAttribute("alLatest", alProduct);
			}
			else{
				ArrayList<Product> alFeature = new ArrayList<Product>();
				ArrayList<Product> alLatest = new ArrayList<Product>();
				for(int i=alProduct.size()-1;i>=alProduct.size()-4;i--){
					alFeature.add(alProduct.get(i));
				}
				for(int i=0;i<4;i++){
					alLatest.add(alProduct.get(i));
				}
				int num = randomNum.random(1, alProduct.size())-1;
				Product product = alProduct.get(num);
				request.setAttribute("product", product);
				request.setAttribute("alFeature", alFeature);
				request.setAttribute("alLatest", alLatest);
				
			}
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}
		else{
			request.setAttribute("error", "Lỗi cơ sở dữ liệu, không có sản phẩm nào để hiển thị.");
			request.getRequestDispatcher("/error.jsp").forward(request, response);
		}	
	}

}
